/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        pulsate: {
          '0%': { transform: 'scale(1)' },
          '25%': { transform: 'scale(.9)' },
          '50%': { transform: 'scale(1)' },
          '75%': { transform: 'scale(1.1)' },
          '100%': { transform: 'scale(1)' },
        },
      },
      animation: {
        'pulsating-hand': 'pulsate 1.5s linear infinite',
      },
    },
    colors: {
      primary: 'rgb(var(--color-white)/75)',
      blueneutral: 'rgb(var(--color-blueneutral)/45)',
      transparent: 'transparent',
      current: 'currentColor',
      'black': '#000000',
      'white': '#ffffff',
      'grey': '#E3E3E3',
      'blue': '#006395',
    },
    fontFamily: {
      'typ1451-medium': ['"Typ1451LLWeb-Medium"'],
      'typ1451-bold': ['"Typ1451LLSub-Bold"'],
      'body': ['"Typ1451LLWeb-Medium"'],
    }
  },
  plugins: [],
}