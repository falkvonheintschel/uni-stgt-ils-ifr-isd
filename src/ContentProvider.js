import React, { useState, createContext,useEffect, useContext } from "react"
import axios from "axios"
import { LanguageContext } from "./LanguageProvider"

const InstituteContext = createContext()

export const ContentProvider = ({ children }) => {

    const lang = useContext(LanguageContext)
    const [instituteData, setInstituteData] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    const setLoading = val => setIsLoading(val)

    useEffect(() => {
        axios.get(`/json/isd-${lang.language}.json`)
            .then((res) => setInstituteData(res.data))
        .catch((err) => console.log(err))
    }, [lang])

    return (
        <InstituteContext.Provider value={{instituteData, isLoading, setLoading}}>
            {children}
        </InstituteContext.Provider>
    )
}

export const useInstitute = () => useContext(InstituteContext)