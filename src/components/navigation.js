import Link from 'next/link'

function Navigation() {
  return (
    <ul className='navigation grid grid-cols-3 grid-rows-2 gap-4 my-10'>
      <li className='p-5 border-2 white'>
        <Link
          href={{
            pathname: '/institut',
            query: { name: 'Institut' },
          }}
          className="block text-center text-4xl uppercase"
        >
          Institut
        </Link>
      </li>

      <li className=' p-5 border-2 white'>
        <Link
          href={{
            pathname: '/news',
            query: { slug: 'news' },
          }}
          className="block text-center text-4xl uppercase"
        >
          News
        </Link>
      </li>
      
      <li className=' p-5 border-2 white'>
        <Link
          href={{
            pathname: '/forschung',
            query: { slug: 'forschung' },
          }}
          className="block text-center text-4xl uppercase"
        >
          Forschung
        </Link>
      </li>
      
      <li className=' p-5 border-2 white'>
        <Link
          href={{
            pathname: '/media',
            query: { slug: 'media' },
          }}
          className="block text-center text-4xl uppercase"
        >
          Mediathek
        </Link>
      </li>

      <li className=' p-5 border-2 white'>
        <Link
          href={{
            pathname: '/rooms',
            query: { slug: 'rooms' },
          }}
          className="block text-center text-4xl uppercase"
        >
          Team / Raumplan
        </Link>
      </li>
      
      <li className=' p-5 border-2 white'>
        <Link
          href={{
            pathname: '/publications',
            query: { slug: 'publications' },
          }}
          className="block text-center text-4xl uppercase"
        >
          Abschlussarbeiten
        </Link>
      </li>
    </ul>
  )
}

export default Navigation