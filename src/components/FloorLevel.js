import { useEffect, useRef, useState } from "react"

export function FloorLevel({clickedTab, floors, floor, room, handleRoom}) {

    const svg = useRef(null)
    
    const [svgUrl, setSvgUrl ] = useState(null)
    const [roomPlanReady, setRoomPlanReady] = useState(false)

    const showRoom = () => {
        clearRooms()

        if (room == null) return
        if (svg.current.contentDocument == null) return

        let group = svg.current.contentDocument.documentElement.getElementById(`${room}`)
        if (group == null) return 

        let rect = group.querySelector('rect')
        let txt = group.querySelector('path')
        let polygon = group.querySelector('polygon')

        if ((txt !== null) && ((rect !== null) || (polygon !== null))) {
            txt.style.fill = '#1C6596'
            if (rect !== null) {
                rect.style.fill = 'white'
            } else {
                polygon.style.fill = 'white'
            }   
        }
    }

    const clearRooms = () => {
        if (svg.current.contentDocument == null) return

        svg.current.contentDocument.documentElement.querySelectorAll('#rooms g').forEach(el => {
            let rect = el.querySelector('rect')
            let txt = el.querySelector('path')
            let polygon = el.querySelector('polygon')

            if ((txt !== null) && ((rect !== null) || (polygon !== null))) {
                txt.style.stroke = 'none'
                txt.style.fill = 'white'
                if (rect !== null) {
                    rect.style.fill = 'transparent'
                    rect.style.stroke = 'none'
                } else {
                    polygon.style.fill = 'transparent'
                    polygon.style.stroke = 'none'
                }
            }
        })
    }

    const addEventListeners = room => {
        clearRooms()
        setRoomPlanReady(true)
        svg.current.contentDocument.documentElement.querySelectorAll('#rooms g').forEach(el => el.addEventListener('click', () => handleRoom(el.id)))
        showRoom(room)
    }
    
    useEffect(() => {
        showRoom(room)
    }, [room])

    useEffect(() => {
        setSvgUrl(floors[floors.findIndex(f => f.nr === floor)].plan) 
        showRoom(room)
    }, [floor])

    return( 
        <>
            <div className={`w-0 h-0 absolute -left-8 ${floor === 0 ? 'bottom-[104px]' : ''} border-t-[26px] border-t-transparent border-r-[32px] border-r-white/[.28] border-b-[26px] border-b-transparent`}
                 style={{ bottom: 24 + (80 * (floor === 0 ? (clickedTab === 'all' ? 4 : 1) : [...floors].reverse().findIndex(f => f.name === floors[floors.findIndex(f => f.nr === floor)].name))) }}
            ></div>

            <object onLoad={() => addEventListeners(room)} ref={svg} data={svgUrl} aria-labelledby={floors[floors.findIndex(f => f.nr === floor)].title} className={`${roomPlanReady ? 'opacity-1' : 'opacity-0'}`} />
        </>
    )
}