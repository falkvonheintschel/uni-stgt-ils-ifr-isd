import React, { useState, useEffect, useContext } from 'react'
import { LanguageContext } from "../LanguageProvider"
import { useInstitute } from '../ContentProvider'
import { useParams } from 'react-router-dom'
import { Pages } from './Pages'
import Loading from './Loading'

function Page() {
    const {instituteData, isLoading, setLoading} = useInstitute()
    const {slug} = useParams()
    const lang = useContext(LanguageContext)
    const page = instituteData?.pages?.filter(page => page.route === slug)
    const [pageContent, setPageContent] = useState(page)
    // const [pageName, setPageName] = useState(null)
    // const [pages, setPages] = useState([])
    
    useEffect(() => {
        // console.log('instituteData', instituteData)
        const page = instituteData?.pages?.filter(page => page.route === slug)
        setLoading(false)
        setPageContent(page)
    }, [instituteData, slug, lang])

    return (
        <main className="relative flex items-center w-screen h-screen overflow-hidden">
            <div className={`container max-w-[1440px] h-[calc(100%-8rem)] mx-auto flex flex-wrap justify-center`}>
                {isLoading && <Loading/>}
                {pageContent && <Pages pageContent={pageContent[0]} />}
            </div>
        </main>
    )
}

export default Page