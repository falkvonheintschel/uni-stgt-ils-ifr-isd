import {Link} from 'react-router-dom';

import ArrowButton from './ArrowButton';

function BackButton() {
    return (
        <div className="absolute bottom-0 left-0 z-20">
            <ArrowButton size={'w-14 h-14'} color={'white'} bg={'transparent'} direction={'down'} rotation={'-rotate-180'}>
              <Link to="/" className="block h-full w-full absolute top-0"></Link>
            </ArrowButton>
        </div>
    )
}

export default BackButton