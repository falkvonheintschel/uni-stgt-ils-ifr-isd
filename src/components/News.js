import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from "axios";
import { useInstitute } from '../ContentProvider';
import { LanguageContext } from '../LanguageProvider';

import BackButton from './BackButton';
import Masonry from 'react-masonry-component';
import { Newsitem } from './Newsitem';
import Loading from './Loading';

const masonryOptions = {
  percentPosition: true,
  transitionDuration: 0,
}

export function News() {
  
  const lang = useContext(LanguageContext)
  const {instituteData, isLoading, setLoading} = useInstitute()
  const [news, setNews] = useState([])
  const [filteredNews, setFilteredNews] = useState([])

  useEffect(() => {
    axios
      .get(`/json/news-${instituteData.shortname}-${lang.language}.json`)
      .then((res) => {
        setLoading(false)
        setNews(res.data.newsItems)
      })
      .catch((err) => console.log(err))
  }, [instituteData, lang])
  
  useEffect(() => {
    const newsSortedDesc = news?.map(obj => ({...obj, date: new Date(obj.date)})).sort((objA, objB) => Number(objB.date) - Number(objA.date))
    // const currentDate = new Date()
    // const filterTimestamp = currentDate.setFullYear(currentDate.getFullYear() - 3)
    // const filterYear = new Date(filterTimestamp)
    // const newsFilteredByDate = newsSortedDesc?.filter(el => Date.parse(el.date) >= Date.parse(filterYear))
    setFilteredNews(newsSortedDesc.length > 20 ? newsSortedDesc.slice(0, 20) : newsSortedDesc)
  }, [news])
  
  // useEffect(() => {
  //   console.log('filteredNews', filteredNews)
  // }, [filteredNews])
  
  return (
    <main className="relative flex items-center w-screen h-screen overflow-hidden">
      <div className="container max-w-[1440px] h-[calc(100%-8rem)] mx-auto flex flex-wrap justify-center">
        <div className="relative h-[calc(100%-86px)] w-full overflow-hidden"> 
          <div className="container max-w-[1280px] h-full mx-auto">
            {isLoading  && <Loading/>}
            
            {filteredNews && <Masonry
              className={'relative news-grid w-full h-full overflow-auto'}
              options={masonryOptions}
              disableImagesLoaded={false}
              updateOnEachImageLoad={false}
            >        
              {filteredNews.map((newsItem, index) => (
                <Newsitem
                  key={index}
                  content={newsItem}
                />
              ))}
            </Masonry>}
          </div>
        </div>

        <div className="relative self-end w-full">
          <BackButton/>

          <div className="container max-w-[1280px] h-full mx-auto flex">
            <div className="w-1/2 py-4 bg-white text-blue">
              <Link to="/news" className="block text-3xl text-center uppercase">News</Link>
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}
