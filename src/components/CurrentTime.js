import { useState, useEffect } from 'react'

function CurrentTime(props) {
  const [dateState, setDateState] = useState(new Date())

  useEffect(() => {
    console.log(new Date().toUTCString())
    setInterval(() => setDateState(new Date()), 1000)
  }, [])

  return (
    <div className="p-4 my-4 border-2 white">
        <p className='text-4xl'>{props.locale}
          <span className='pl-2'>
            {
              props.timezone ? 
                dateState.toLocaleString(props.timezone, {
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: false,
                }) : 
                `${dateState.getUTCHours()}:${String(dateState.getUTCMinutes()).padStart(2, '0')}`
            }
          </span>
        </p>
    </div>
  )
}

export default CurrentTime