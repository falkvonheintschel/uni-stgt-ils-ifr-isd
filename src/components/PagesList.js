import ArrowButton from './ArrowButton'

export function PagesList({ pages, handleTabClick, handleSubTabClick, currentPage, currentSubPage }) {

    // useEffect(() => {
    //     console.log('pages', pages)
    // }, [pages])
    
    return (
        <ul className='block'>
            {pages?.map((p, i) => {
                if (Object.keys(p).length > 0) {
                    return (
                        <li 
                            key={i} 
                            className={`my-3`}
                            disabled={currentPage?.name === `${p.name}`}
                            onClick={() => handleTabClick(p)}
                        >
                            <div className='relative flex flex-nowrap'>
                                <ArrowButton 
                                    isActive={currentPage?.name === `${p.name}` ? true : false} 
                                    size={'w-12 h-12'} color={currentPage?.name === `${p.name}` ? 'none' : 'white'} 
                                    bg={currentPage?.name === `${p.name}` ? 'white' : 'transparent'} 
                                    rotation={currentPage?.name === `${p.name}` && currentPage.subpages.length > 0 ? 'rotate-90' : 'rotate-0'} 
                                />

                                <span className='self-center block w-4/5 mx-6 text-2xl text-left text-white uppercase'>
                                    {p.name}
                                </span>
                            </div>

                            {(currentPage?.name === p.name &&  p?.subpages?.length >= 1) &&
                                <ul className='pl-20'>
                                    {p.subpages.map((sp, j) => {
                                        return (
                                            <li
                                                key={`subpage-${j}`}
                                                disabled={currentPage?.name === sp.name}
                                                onClick={e => handleSubTabClick(sp, e)}
                                                className={currentSubPage?.name === sp.name ? 'my-1 font-bold' : 'my-1'}
                                            >
                                                <div className='relative flex items-center flex-nowrap'>
                                                    <span className='mx-2 text-lg text-left text-white'>{sp.name}</span>
                                                </div>
                                            </li>
                                        )
                                    })}
                                </ul>
                            }
                        </li>
                    ) 
                }           
            })}
        </ul>
    )       
}