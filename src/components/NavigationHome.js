import React from "react";
import { Link } from 'react-router-dom';
import { useInstitute } from "../ContentProvider";
import LanguageSelector from './LanguageSelector';

export function NavigationHome() {

    const {instituteData} = useInstitute()

    return (
        <nav className="relative container mx-auto max-w-[1280px] z-10 self-end">
            <div className="absolute bottom-0 inset-x-0 container max-w-[1440px] mx-auto">
                <div className="absolute bottom-0 z-20 -left-28">
                    <LanguageSelector />
                </div>
            </div>
            
            <ul className='grid grid-cols-3 grid-rows-2 navigation gap-x-5 gap-y-4'>
                {instituteData.pages?.map((page, i) => (
                    page.subpages !== null ?
                        <li key={i} className='p-3 border-2 white'>
                            <Link 
                                to={page.url === '' ? `/${page.route}` : `/page/${page.route}`} 
                                className="block text-3xl text-center uppercase"
                                state={page.route === 'media' ? {filter: page.navigationTitle !== 'Mediathek' && page.navigationTitle !== 'Media' ? page.navigationTitle : null} : null}
                            >
                                {page.navigationTitle}
                            </Link> 
                        </li> : null
                    ))
                }
            </ul>
        </nav>
    )
}