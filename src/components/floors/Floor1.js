import { useState, useEffect, useContext } from 'react';
import { LanguageContext } from "../LanguageProvider"
import axios from "axios";

import { TeamList } from './TeamList';
import { Floor1Svg } from './Floor1Svg'

export function Floor1() {

    const lang = useContext(LanguageContext)

    const [room, setRoom] = useState(null);
    const [people, setPeople] = useState([]);
    const [filteredPeople, setFilteredPeople] = useState([]);
    
    const handleRoom = room => {
        setRoom(room);
        setFilteredPeople(people.filter(person => person.room === room))
        // console.log(filteredPeople)
    }

    const handlePerson = room => {
        setRoom(room);
    }

    // const handleRoom = room => setFilteredPeople(filteredPeople.filter(person => person.room === room))
    
    useEffect(() => {
        // console.log(room);
    }, [room]);

    useEffect(() => {   
        axios
            .get(`/json/ils-${lang.language}.json`)
            .then((res) => {
                setFilteredPeople(res.data)
                setPeople(res.data)
            })
            .catch((err) => console.log(err));
    }, [lang]);
    
    return (
        <div className='flex justify-between h-full overflow-hidden text-white'>

            <TeamList personData={filteredPeople} handlePerson={handlePerson} />

            <div className='w-[calc(50%-0px)] relative h-full py-3 bg-white/[.28]'>
                <div className='w-0 h-0 absolute -left-8 bottom-24
                    border-t-[26px] border-t-transparent
                    border-r-[32px] border-r-white/[.28]
                    border-b-[26px] border-b-transparent'>
                </div>

                <Floor1Svg room={room} handleRoom={handleRoom} handlePerson={handlePerson} />
            </div>
        </div>    
    )
}