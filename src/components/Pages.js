import { useState, useEffect, useRef } from 'react'
import ArrowButton from './ArrowButton'
import BackButton from './BackButton'
import Loading from './Loading'
import { useInstitute } from '../ContentProvider'
import { PagesList } from './PagesList'

export function Pages({pageContent}) {

    const iframe = useRef(null)

    const {instituteData} = useInstitute()
    const [basePage, setBasePage] = useState(pageContent)
    const [pages, setPages] = useState([])
    const [currentPage, setCurrentPage] = useState(pageContent)
    const [currentSubPage, setCurrentSubPage] = useState()
    const [iFrameLoaded, setIFrameLoaded] = useState(false)

    const handleTabClick = page => {
        // console.log('page', page.name, 'currentPage', currentPage?.name)
        currentPage?.name !== `${page.name}` && setIFrameLoaded(false)
        setCurrentPage(page)
        setCurrentSubPage(null)
    }

    const handleSubTabClick = (page, e) => {
        e.stopPropagation()
        // console.log('page', page.name, 'currentPage', currentPage?.name)
        setIFrameLoaded(false)
        setCurrentSubPage(page)
    }

    useEffect(() => {
        // console.log('basePage', basePage)        
        setPages(basePage?.subpages)
    }, [basePage])
    
    // useEffect(() => {
    //     console.log('currentPage', currentPage)                
    // }, [currentPage])    

    return (
        <>
            <div className={`relative h-[calc(100%-86px)] w-full overflow-hidden border-2 border-r-2 border-white p-6 ${instituteData.shortname === 'isd' ? 'bg-blue' : ''} page-container`}> 
                <div className="h-full container max-w-[1280px] mx-auto"> 
                    <div className='flex h-full flex-nowrap'>
                        <div className='flex flex-col justify-end w-1/2 h-full space-x-1'>
                            <ul className='block'>
                                {currentPage !== null && <PagesList pages={pages} handleTabClick={handleTabClick} handleSubTabClick={handleSubTabClick} currentPage={currentPage} currentSubPage={currentSubPage} />}
                                {/* {currentPage !== null && listSubpages} */}
                            </ul>
                        </div>

                        <div className={`w-1/2`}>
                            <div className='relative w-full h-full overflow-hidden text-white'>
                                {!iFrameLoaded && <Loading/>}
                                
                                {(currentPage !== null || currentSubPage !== null) && 
                                    <div className={`relative z-0 h-full bg-transparent ${iFrameLoaded ? 'opacity-1 iframe-loaded' : 'opacity-0'} transition-opacity delay-500`}>
                                        <iframe
                                            ref={iframe}
                                            className={`w-full h-full`}
                                            onLoad={() => setIFrameLoaded(true)}
                                            title={currentSubPage != null ? currentSubPage.url : currentPage?.url} 
                                            src={currentSubPage != null ? currentSubPage.url : currentPage?.url}
                                        />
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="relative self-end w-full">
                <BackButton/>
                
                <div className="container max-w-[1280px] h-full mx-auto flex">
                    <div className="w-1/2 py-3 bg-white text-blue" onClick={((e) => handleTabClick(pageContent, e))}>
                        <span className="block text-3xl text-center uppercase">{pageContent?.name}</span>
                    </div>
                </div>
            </div>
        </>
    )       
}


// useEffect(() => {
//     // iframe?.current.addEventListener('load', () => setIFrameLoaded(true))
// }, [currentSubPage])

// const listSubpages = basePage?.subpages.map((page, i) => {
//     if (Object.keys(page).length > 0) {
//         return (
//             <li 
//                 key={i} 
//                 className={`my-3`}
//                 disabled={currentPage?.name === `${page.name}`}
//                 onClick={() => handleTabClick(page)}
//             >
//                 {(i >= 0) && 
//                     <div className='relative flex flex-nowrap'>
//                         <ArrowButton 
//                             isActive={currentPage?.name === `${page.name}` ? true : false} 
//                             size={'w-12 h-12'} color={currentPage?.name === `${page.name}` ? 'none' : 'white'} 
//                             bg={currentPage?.name === `${page.name}` ? 'white' : 'transparent'} 
//                             rotation={currentPage?.name === `${page.name}` && currentPage.subpages.length > 0 ? 'rotate-90' : 'rotate-0'} 
//                         />
//                         <span className='self-center block w-4/5 mx-6 text-2xl text-left text-white uppercase'>{page.name}</span>
//                     </div>
//                 }

//                 {(currentPage?.name === page.name && page.subpages.length > 0) && 
//                     <ul className='pl-20'>
//                         {page.subpages?.map((subpage, j) => 
//                             <li
//                                 key={`subpage-${j}`}
//                                 disabled={currentPage?.name === subpage.name}
//                                 onClick={e => handleSubTabClick(subpage, e)}
//                                 className={currentSubPage?.name === subpage.name ? 'my-1 font-bold' : 'my-1'}
//                             >
//                                 <div className='relative flex items-center flex-nowrap'>
//                                     <span className='mx-2 text-lg text-left text-white'>{subpage.name}</span>
//                                 </div>
//                             </li>
//                         )}
//                     </ul>
//                 }
//             </li>
//         )
//     } 
// })