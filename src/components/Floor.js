import axios from 'axios'
import { useState, useEffect, useRef, useContext } from 'react'
import { LanguageContext } from "../LanguageProvider"
import { TeamList } from './TeamList'
import { FloorLevel } from './FloorLevel'
import { useInstitute } from '../ContentProvider'
import Loading from './Loading'

export function Floor({floors, floor, clickedTab, setSelectedIndex, activePerson, setActivePerson, room, setRoom}) {
    
    const {instituteData, isLoading, setLoading} = useInstitute()
    const lang = useContext(LanguageContext)

    const activeRoom = useRef()
    activeRoom.current = room

    const [people, setPeople] = useState([])
    const [filteredPeople, setFilteredPeople] = useState([])
    const [rooms, setRooms] = useState([])
    const [filteredRooms, setFilteredRooms] = useState([])

    const checkActive = el => activePerson.some(ap => ap.name === el.name)

    const handleRoom = newRoom => {
        console.log('FLOOR : handleRoom --> ', newRoom)
        // setSelectedIndex(floors.findIndex(f => f.name === newRoom.charAt(0) === 'V' ? '0' : newRoom.charAt(0)))
        if (newRoom !== activeRoom.current) {
            setRoom(newRoom)
        }        
        setActivePerson(filteredPeople?.filter(person => activeRoom.current === newRoom ? null : person.room === newRoom))
    }

    const handlePerson = person => {
        console.log('handlePerson --> name: ', person.name, ' | room:', person.room, ' | personFloor:', person.floor)
        setSelectedIndex(floors.findIndex(f => f.name === person.floor))
        if (person.role !== 'room' && activePerson.length === 1 && activePerson[0].name === person.name ) {
            setRoom(null)
        } else {
            handleRoom(person.room)
        }
        const isActive = checkActive(person)
        setActivePerson(isActive ? [] : [person])
        // setActivePerson(people?.filter(person => roomForFilter == null ? person.floor === floor.toString() : person.room === roomForFilter))
    }

    useEffect(() => {
        console.log('activePerson', activePerson)
    }, [activePerson])

    useEffect(() => {
        // console.log('instituteData', instituteData)
        if (instituteData.length !== 0) {
            const peopleRoomData = [
                `/json/${instituteData?.shortname}-team-${lang.language}.json`,
                `/json/institutes-rooms-${lang.language}.json`
            ]

            if (peopleRoomData !== null) {
                Promise.all(peopleRoomData?.map(data => axios.get(data))).then(( [{ data: institutePeople }, { data: rooms }] ) => {
                    setPeople([...new Map(institutePeople?.map(p => [p.name, p])).values()])
                    setRooms(rooms.filter(r => (r.institute === '' || r.institute === instituteData?.shortname)))
                })            
            }
        }

        
    }, [instituteData, lang])

    useEffect(() => {
        setLoading(false)
        setFilteredPeople(clickedTab === 'all' ? people : people?.filter(person => person.floor === floor.toString()))
        setFilteredRooms(clickedTab === 'all' ? rooms : rooms?.filter(room => room.floor === floor.toString()))

        // setFilteredPeople(people?.filter(person => person.room === roomForFilter))
        // setFilteredRooms(rooms?.filter(room => room.room === roomForFilter))
    }, [people, rooms])

    useEffect(() => {
        if (clickedTab === 'all') {
            setActivePerson([])
            setRoom(null)
        }
        // setFilteredPeople(people?.filter(person => person.floor === floor.toString()))
    }, [clickedTab]) 
    
    return (
        <div className={`flex justify-between h-full text-white floor-${floor}`}>
            {isLoading && <Loading/>}
            
            {(filteredPeople || filteredRooms) && 
                <TeamList 
                    personData={filteredPeople} 
                    roomData={filteredRooms} 
                    handlePerson={handlePerson} 
                    floor={floor} 
                    activePerson={activePerson} 
                    room={room} 
                />}
            
            <div className='w-[calc(50%-0px)] relative h-full py-3 bg-white/[.28]'>
                <FloorLevel 
                    clickedTab={clickedTab}
                    floors={floors} 
                    floor={floor} 
                    room={room} 
                    handleRoom={handleRoom} 
                /> 
            </div>
        </div>    
    )
}