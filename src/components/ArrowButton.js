function ArrowButton(props) {
    return (
        <span className={`block overflow-hidden ${props.size ? props.size : 'w-8 h-8'} ${props.rotation ? props.rotation : null} text-sm ${props.color ? 'text-'+props.color : 'text-white'} border-${props.isActive ? '0' : '2'} rounded-full`}>
            <svg xmlns="http://www.w3.org/2000/svg" 
                width="100%"
                height="100%" 
                viewBox="0 0 24 24"
            >
                {props.isActive ? (
                    <path paintOrder="fill" fill={`${props.isActive ? 'white' : 'transparent'}`} d="M 0.039 -0.056 L 24.01 -0.056 L 24.01 23.989 L 0.039 23.989 Z M 8.487 6.937 L 13.551 12 L 8.487 17.063 L 9.469 18.045 L 15.513 12 L 9.469 5.955 Z"/>
                ) : (
                    <polygon fill="white" points="8.487 6.937 13.551 12 8.487 17.063 9.469 18.045 15.513 12 9.469 5.955 8.487 6.937"/>
                )}
            </svg>
            {props.children}
        </span>
    )
}

export default ArrowButton