import { useContext, useEffect, useState } from 'react';
import { useInstitute } from '../ContentProvider';
import { LanguageContext } from '../LanguageProvider';

export function TeamList({personData, roomData, room, activePerson, handlePerson}) {
    
    const { instituteData } = useInstitute()
    const lang = useContext(LanguageContext)

    const [roomsInstitute, setRoomsInstitute] = useState([])
    const [roomsFaculty6, setRoomsFaculty6] = useState([])

    const checkActive = el => activePerson.some(ap => ap.name === el.name)
    
    // useEffect(() => {
    //     console.log('TEAMLIST --> activePerson: ', activePerson)
    // }, [personData])
    
    useEffect(() => {
        // console.log('TEAMLIST --> roomData', roomData)
        setRoomsInstitute(roomData?.filter(r => r.institute === instituteData?.shortname))
        setRoomsFaculty6(roomData?.filter(r => r.institute === ''))
    }, [roomData])

    return (
        <div className='w-[calc(50%-160px)] h-full pt-5 px-5 pb-10 bg-white/[.28] relative overflow-hidden'>
            <h3 className='mb-2 text-base font-medium'>
                <span className='uppercase'>Team</span><br/>
                {instituteData?.institute} <span className='uppercase'>({instituteData?.shortname})</span>
            </h3>
            
            <div className='w-full h-[95%] relative overflow-y-scroll overscroll-contain'>
                <ul className='block'>
                    {personData != null && personData?.map((person, i) => {
                        const isActive = checkActive(person)
                        return (
                            <li key={i}
                                className={`my-2 ${isActive ? 'font-extrabold' : 'font-medium'}`} 
                                id={person.room}
                                onClick={() => handlePerson(person)}
                            >
                                <div className='flex gap-5'>
                                    <p className='text-sm leading-5 basis-1/5 w-14'>{person.room}</p>
                                    
                                    <div className='basis-4/5 shrink'>
                                        <p className='text-sm leading-5'>{person.name} / {person.title}</p>
                                        <p className='text-xs' dangerouslySetInnerHTML={{ __html: person.role }} />
                                    </div>
                                </div>
                            </li>
                        )
                    })}
                </ul>
                    
                {roomsInstitute.length !== 0 &&
                    <>
                        <h3 className='mt-6 mb-2 text-base font-medium'>
                            <span className='uppercase'>{lang.language === 'de' ? 'Funktionsräume' : 'Functional rooms'}</span><br/>
                            {instituteData.institute} <span className='uppercase'>({instituteData.shortname})</span>
                        </h3>
                        
                        <ul className='block'>
                            {roomsInstitute?.map((r, i) => 
                                <li key={i}
                                    className={`${i === roomsInstitute.length-1 ? 'mt-2 mb-2' : 'my-2'}`}
                                    id={r.room}
                                    onClick={() => handlePerson(r)}
                                >
                                    <div className={`flex gap-5`}>
                                        <p className={`text-sm ${r.room === room ? 'font-extrabold' : 'font-medium'} leading-5 basis-1/5 w-14`}>{r.room}</p>
                                        
                                        <div className='basis-4/5 shrink'>
                                            <p className={`text-sm ${r.room === room ? 'font-extrabold' : 'font-medium'} leading-5`}>{r.name}</p>
                                        </div>
                                    </div>
                                </li>
                            )}
                        </ul>
                    </>}

                {roomsFaculty6.length !== 0 &&
                    <>
                        <h3 className='mt-6 mb-2 text-base font-medium'>
                            <span className='uppercase'>{lang.language === 'de' ? 'Funktionsräume' : 'Functional rooms'}</span><br/> {lang.language === 'de' ? 'Fakultät 6' : 'Faculty 6'}
                        </h3>

                        <ul className='block'>
                            {roomsFaculty6?.map((r, i) => 
                                <li key={i}
                                    className={`${i === roomsFaculty6.length-1 ? 'mt-2 mb-2' : 'my-2'}`}
                                    id={r.room}
                                    onClick={() => handlePerson(r)}
                                >
                                    <div className={`flex gap-5`}>
                                        <p className={`text-sm ${r.room === room ? 'font-extrabold' : 'font-medium'} leading-5 basis-1/5 w-14`}>{r.room}</p>
                                        
                                        <div className='basis-4/5 shrink'>
                                            <p className={`text-sm ${r.room === room ? 'font-extrabold' : 'font-medium'} leading-5`}>{r.name}</p>
                                        </div>
                                    </div>
                                </li>
                            )}
                        </ul>
                    </>}
            </div>
        </div>
    )
}