import React, { useContext, useEffect, useState } from 'react'
import Modal from './Modal'
import 'yet-another-react-lightbox/styles.css'
import 'video-react/dist/video-react.css'

import pictoBild from '../Pikto_Bild.png'
import pictoText from '../Pikto_Text.png'
import pictoVideo from '../Pikto_Video.png'
import { LanguageContext } from '../LanguageProvider'

export function MediaItem({content}) { 

    const lang = useContext(LanguageContext)

    const [isOpen, setIsOpen] = useState(false)
    const [hasPreviewImg, setHasPreviewImg] = useState(content?.suffix?.value === 'jpg' ? true : false)
    const [title, setTitle] = useState(null)

    useEffect(() => {
        if (content.title.de.value === 'F&lt;span class="hideonmobile"&gt;akultät &lt;/span&gt;06' || content.title.en.value === 'F&lt;span class="hideonmobile"&gt;aculty &lt;/span&gt;06') {
            setTitle(content.title.fb.value)
        } else {
            setTitle(lang.language === 'de' ? content.title.de.value : content.title.en.value)
        }
    }, [content])
    
    return (
        <div className="absolute w-[calc(25%-10px)] text-black media-item mb-2 overflow-hidden">            
            
            <Modal content={content} type={'media'} handleClose={() => setIsOpen(false)} isOpen={isOpen}>
                <div className={`relative w-full bg-blueneutral ${!hasPreviewImg ? 'h-48' : ''}`} onClick={() => setIsOpen(true)}>
                    <div className={`relative z-0 w-full ${hasPreviewImg ? 'h-full' : 'h-48'}`}>
                        {( hasPreviewImg && <img src={content.link.value} className='block w-full h-full' alt={title} /> )} 
                    </div>
                    
                    <div className={`absolute top-0 z-10 flex content-center justify-start p-3 flex-nowrap w-full bg-blueneutral`}>
                        <div className="relative basis-6 h-7 icon">
                            {content?.suffix?.value === 'jpg' && <img src={pictoBild} className='object-contain w-full h-full' alt=''/>   }
                            {content?.suffix?.value === 'mp4' && <img src={pictoVideo} className='object-contain w-full h-full' alt=''/>   }
                            {content?.suffix?.value === 'pdf' && <img src={pictoText} className='object-contain w-full h-full' alt=''/>   }
                        </div>

                        <h3 className='relative z-0 w-3/4 ml-4 text-white shrink text-md'>
                            {title}                            
                        </h3>
                    </div>
                </div>
            </Modal>

        </div>
    )
}

