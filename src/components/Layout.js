import React from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import ActivityDetector from 'react-activity-detector'

export function Layout() {

  const navigate = useNavigate()
  
  const customActivityEvents = [
    'click',
    'keydown',
    'mousemove',
    'touchstart', 
    'touchmove',
  ]

  return (
    <>
      <ActivityDetector activityEvents={customActivityEvents} enabled={true} timeout={600000} onIdle={() => navigate('/', { replace: true })} />

      <Outlet />
    </>
  )
}

export default Layout