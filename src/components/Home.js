import React, { useEffect, useState } from "react";
import XMLParser from 'react-xml-parser';
import { NavigationHome } from './NavigationHome';
import CurrentTime from './CurrentTime';
import hand from './../hand.svg';
import videoILS from './../bg_ils.mp4';
import videoISD from './../bg_isd.mp4';
import videoIFR from './../bg_ifr.mp4';
import { useInstitute } from "../ContentProvider";
import axios from "axios";
import { HomeInfo } from "./HomeInfo";

const bgVideos = {
    ils: videoILS,
    isd: videoISD,
    ifr: videoIFR
}

export function Home() {

    const {instituteData} = useInstitute()
    const [info, setInfo] = useState([])
    const [infoModal, setInfoModal] = useState([])
    const [showInfo, setShowInfo] = useState(false)

    useEffect(() => {   
        const getData = async () => {
            const data = await axios
                .get('/json/xml_modal.xml')
                .then((res) => {
                    const jsonDataFromXml = new XMLParser().parseFromString(res.data)
                    setInfo(jsonDataFromXml.getElementsByTagName('doc'))
                })
                .catch((err) => console.log(err))
        }
        getData()
    }, [])

    useEffect(() => {
        // console.log('info', info)
        const currentItems = info?.filter(obj => {
            const values = Object.values(obj.children)
            const dateNow = Date.now()
            const dateReleasedObj = values.find((obj) => obj.attributes.name === 'released')
            const dateReleased = new Date(dateReleasedObj.value)
            const dateReleasedTS = dateReleased.getTime()
            const dateExpiredObj = values.find((obj) => obj.attributes.name === 'expired')
            const dateExpired = new Date(dateExpiredObj.value)
            const dateExpiredTS = dateExpired.getTime()            
            if (dateNow >= dateReleasedTS && dateNow <= dateExpiredTS) {
                return obj
            }
        }).map (obj => obj)
        // console.log('currentItems', currentItems)

        const infoContent = currentItems.map(item => item.children.find((txt) => txt.attributes.name === 'content'))
        setInfoModal(infoContent)        
    }, [info])
    
    useEffect(() => {
        // console.log('infoModal', infoModal)
        setShowInfo(infoModal.length > 0 ? true : false)
    }, [infoModal])

    return (
        <main className="relative flex items-center w-screen h-screen overflow-hidden">
            <div className="container max-w-[1440px] mx-auto mb-4 flex flex-wrap justify-center h-full m-0 py-16">
                <div className="absolute inset-0 pointer-events-none -z-1">
                    <video
                        className="absolute object-cover w-full h-full"
                        playsInline
                        autoPlay
                        loop
                        muted
                        alt="Animation"
                        src={bgVideos[instituteData.shortname]}
                    />
                </div>

                <div className="absolute w-auto top-12 right-16">
                    <CurrentTime locale='UTC'/>
                    <CurrentTime timezone='de-DE' locale='STR'/>
                    {/* Europe/Berlin timezone='en-GB' */}
                </div>
                
                {info.length > 0 && showInfo && 
                    <div className="absolute inset-0 w-full h-full">
                        <HomeInfo handleClose={() => setShowInfo(false)} showInfo={showInfo} infoModal={infoModal} />

                        <div className="absolute inset-x-0 w-32 h-32 p-2 m-auto border-2 border-white rounded-full pointer-events-none hand bottom-28 animate-pulsating-hand">
                            <div className="relative w-full h-full p-6 border-2 border-white rounded-full">
                                <img src={hand} className='relative block object-contain w-full h-full m-auto' alt="klick" />
                            </div>
                        </div>
                    </div>
                }

                {!showInfo && 
                    <>
                        <div className="relative container max-w-[1280px] mx-auto w-full z-10 self-center">
                            <h1 className="relative text-[40vh] leading-none z-10 uppercase">{instituteData.shortname}</h1>
                        </div>

                        <NavigationHome/>
                    </>    
                }
            </div>
        </main>
    )
}