import React, { useEffect } from "react";
import { useState } from 'react';
import Modal from '../components/Modal'
import ModalContent from "./ModalContent";

export function Newsitem(content) { 

    const [isOpen, setIsOpen] = useState(false)
    const [date, setDate] = useState(new Date(content.content.date))
    
    return (
        <div className="absolute w-1/3 p-1 text-black xl:w-1/4 news-item">
            
            <Modal content={content.content} type={'news'} handleClose={() => setIsOpen(false)} isOpen={isOpen}>
                <div className="w-full p-8 pt-12 bg-white" onClick={() => setIsOpen(true)}>
                    
                    {content.content.image !== undefined &&
                        <div className='flex content-center justify-center w-full mb-8'>
                            <img src={content.content.image} className='block w-2/3 border-4 rounded-full bg-grey border-blue' alt={content.content.headline}/>
                        </div> }

                    <p className='mb-3 text-sm text-black'>
                        {date.toLocaleString('de-DE', {
                            year: 'numeric', 
                            month: 'long', 
                            day: 'numeric'
                        })}
                    </p>
                    
                    <h3 className='text-2xl font-light text-black break-words'>
                        {content.content.headline}
                    </h3>
                </div>
            </Modal>
            
        </div>
    );
}