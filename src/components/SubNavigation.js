function SubNavigation(props) {

    const subpages = props.projects

    return (
        // ${props.color ? 'text-'+props.color : 'text-white'}
        <div className='ml-8'>
            <ul>
                {subpages.map((subpage, index) =>
                    <li className='' key={index}>
                        <span 
                            onClick={ 
                                (e) => {
                                    e.stopPropagation()
                                    console.log(e.currentTarget.textContent)
                                }
                            } 
                            className='text-2xl text-white mb-2'>{subpage.name}</span>
                        {/* <p>{el.text ? el.text : null}</p> */}
                    </li>
                )}
            </ul>
        </div>
    )
}

export default SubNavigation