import React from "react"
import ReactPortal from './ReactPortal'
import ModalContent from "./ModalContent"
import ArrowButton from "./ArrowButton"

export default function Modal({ children, content, type, isOpen, handleClose }) {
    // console.log(content)
    return (
        <>
            {children}
                
            {isOpen ? (
                <ReactPortal wrapperId="react-portal-modal-container">
                    <div className="fixed inset-0 z-10 block overflow-hidden">
                        <div className="absolute inset-0 z-0" onClick={handleClose}>
                            <button onClick={handleClose} className="absolute bottom-16 left-[270px] z-60">
                                <ArrowButton size={'w-14 h-14'} color={'white'} bg={'transparent'} rotation={'rotate-180'} />
                            </button>
                        </div>


                        {/* <div className="fixed inset-0 z-10 flex flex-wrap items-center justify-center"> */}
                        <div className={`block absolute inset-0 z-10 container ${content.suffix?.value === 'mp4' ? 'max-w-7xl' : 'max-w-4xl'} m-auto overflow-hidden`}> 
                            <ModalContent content={content} type={type}/>
                        </div>
                    </div>
                </ReactPortal>
            ) : null} 
        </>
    )
}
