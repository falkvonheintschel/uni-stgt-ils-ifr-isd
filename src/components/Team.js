import React, { useContext, useState } from "react"
import { Link } from 'react-router-dom'
import { Tab } from '@headlessui/react'
import { LanguageContext } from "../LanguageProvider"
import { Floor } from './Floor'
import ArrowButton from './ArrowButton'

import FloorUG  from './floors-svg/Grundriss_UG.svg'
import FloorEG  from './floors-svg/Grundriss_EG.svg'
import Floor1  from './floors-svg/Grundriss_1OG.svg'
import Floor2  from './floors-svg/Grundriss_2OG.svg'

function classNames(...classes) {
    return classes.filter(Boolean).join(" ")
}

export function Team() {
    const lang = useContext(LanguageContext)
    
    const floorPlans = [
        { name: 'all', nr: 0, title: 'All', plan: FloorEG },
        { name: '2', nr: 2, title: '2. OG', plan: Floor2 },
        { name: '1', nr: 1, title: '1. OG', plan: Floor1 },
        { name: '0', nr: 0, title: 'EG', plan: FloorEG },
        { name: '-1', nr: -1, title: 'UG', plan: FloorUG }
    ]
    const [floors] = useState(floorPlans)
    const [selectedIndex, setSelectedIndex] = useState(0)
    const [clickedTab, setClickedTab] = useState(null)
    const [activePerson, setActivePerson] = useState(null)
    const [room, setRoom] = useState(null)
    
    // useEffect(() => {
    //     console.log('TEAM --> room: ', room, 'TEAM --> activePerson: ', activePerson)
    // }, [activePerson, room])
    
    return (
        <main className="relative flex items-center w-screen h-screen overflow-hidden">
            <div className="container max-w-[1280px] h-[calc(100%-8rem)] mx-auto flex flex-wrap justify-center ">
                <div className="relative h-[calc(100%-86px)] w-full overflow-hidden"> 
                    <Tab.Group vertical selectedIndex={selectedIndex} onChange={setSelectedIndex}> 
                        <Tab.List className="absolute left-0 z-10 flex flex-col justify-end w-16 h-full mx-auto right-40">
                            {floors.map((floor, i) => 
                                <Tab key={i} className={({selected}) => classNames("block text-4xl my-2 w-16 h-16 border-2 rounded-full", selected ? "bg-white border-transparent text-blue" : "bg-transparent border-white")}>
                                    {i === 0 ? <span style={{fontSize: '24px', lineHeight: 2}}>{lang.language === 'de' ? 'Alle' : 'All'}</span> : floor.nr}
                                </Tab>
                            )}
                        </Tab.List>
                        
                        <Tab.Panels className="absolute z-0 w-full h-full">
                            {floors.map((floor, i) => 
                                <Tab.Panel key={i} className='block w-full h-full my-2 text-sm bg-transparent'>
                                    <Floor 
                                        floors={floors} 
                                        floor={floor.nr} 
                                        clickedTab={floors[selectedIndex].name} 
                                        setSelectedIndex={setSelectedIndex} 
                                        activePerson={activePerson} 
                                        setActivePerson={setActivePerson} 
                                        room={room} 
                                        setRoom={setRoom}
                                    />
                                </Tab.Panel>
                            )}
                        </Tab.Panels>
                    </Tab.Group>
                </div>
                
                <div className="relative self-end w-full">
                    <div className="absolute bottom-0 z-20 -left-20">
                        <ArrowButton size={'w-14 h-14'} color={'white'} bg={'transparent'} direction={'down'} rotation={'-rotate-180'}>
                            <Link to="/" className="absolute top-0 block w-full h-full"></Link>
                        </ArrowButton>
                    </div>

                    <div className="container max-w-[1280px] h-full mx-auto flex">
                        <div className="w-1/2 py-3 bg-white text-blue">
                            <span className="block text-3xl text-center uppercase"
                                  onClick={() => {
                                    setSelectedIndex(0)
                                    setClickedTab(null)
                                  }}
                            >
                                {lang.language === 'de' ? 'Team / Raumplan' : 'Team / Floorplan'}
                            </span>
                        </div>
                    </div>                
                </div>
            </div>            
        </main>        
    )
}

export default Team