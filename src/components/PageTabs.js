import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

export function PageTabs(props) {

    const pages = props.content;

    const listTabs = pages.map((page, index) =>
        <Tab
            key={index}
        >
            <div className='flex flex-nowrap items-center relative'>
                {/* <ArrowButton size={'w-6 h-6'} color={selected ? 'none' : 'white'} bg={selected ? 'white' : 'transparent'} rotation={selected ?'rotate-90' : '-rotate-90'} /> */}
                <span className='mx-2 text-2xl text-white uppercase text-left'>{page.name}</span>
            </div>
        </Tab>
    );

    const listPanels = pages.map((page, index) =>
        <TabPanel
            key={index}
            className="block h-full w-1/2 relative"
            // className={() => classNames("block my-2 w-full h-full bg-transparent text-sm")}
        >
            <div className='w-full h-full overflow-auto'>
                <h2 className='text-2xl text-white font-bold mb-2'>{page.name}</h2>

                <iframe
                    className='w-full h-[calc(100%-80px)]'
                    title={page.title}
                    src={page.url}
                    width="100%"
                    height="100%"
                ></iframe>
            </div>
        </TabPanel>
    )

    return (
        <>
            <div className="container max-w-[1440px] mx-auto mb-4 flex flex-wrap justify-center h-[calc(100%-180px)] m-0 pt-16">
                <div className="relative h-full w-full overflow-hidden">
                    <Tabs className="relative w-full h-full flex flex-wrap">
                        
                        <TabList className="h-full w-1/2 flex justify-end flex-col space-x-1 "> 
                            {listTabs} 
                        </TabList>

                        {listPanels}

                        {/* <TabPanel>
                            <Tabs defaultIndex={1}>
                                <TabList>
                                    {pages.projects.content.map((el, index) =>
                                        <Tab className='' key={index}>
                                            {el.name}
                                        </Tab>
                                    )}
                                    
                                </TabList>
                            </Tabs>
                        </TabPanel> */}
                    </Tabs>
                </div>
            </div>
        </>
    )
}