import React, { useEffect } from "react";
import { useState } from 'react';
import { Player } from 'video-react';
import { Document, Page, pdfjs} from 'react-pdf';
import 'react-pdf/dist/esm/Page/TextLayer.css';

export default function ModalContent(props) {
    
    const [numPages, setNumPages] = useState(null)
    const [pageNumber, setPageNumber] = useState(1)
    // const [date, setDate] = useState(new Date(props.content.date))

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages)
    }

    useEffect(() => {
        // console.log(props)
        pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.js`;
    }, [])
    
    if (props.type === 'news') {
        const date = new Date(props.content.date)

        return (
            <div className="absolute inset-x-0 z-20 bg-white shadow-lg top-16 bottom-16">
                <div className="relative flex flex-wrap items-center justify-center w-full h-full overflow-hidden">
                    <div className="relative h-full px-24 py-12 overflow-auto">

                        {props.content.image !== undefined && 
                            <div className='flex w-1/4 mb-12'>
                                <img src={props.content.image} className='block w-full rounded-full bg-grey border-3 border-blue' alt={props.content.headline}/>
                            </div>}
                        
                        <p className='mb-3 text-base text-black'>
                            {date.toLocaleString('de-DE', {
                                year: 'numeric', 
                                month: 'long', 
                                day: 'numeric'
                            })}
                        </p>

                        <h1 className="text-3xl font-medium text-black">{props.content.headline}</h1>

                        <div className='my-8 text-black' dangerouslySetInnerHTML={{ __html: props.content.content }} />                                
                    </div>
                </div>
            </div>
        )
    } else {

        switch(props.content.suffix.value) {
            case 'jpg':

                const date = new Date(props.content.date.value)

                return (
                    <div className="relative z-20 h-full shadow-lg">
                        <div className="relative flex flex-wrap items-center justify-center h-full">
                            <div className="relative w-full h-full p-4 overflow-hidden bg-white">
                                {props.content.link.value !== undefined && <img src={props.content.link.value} className='block object-contain w-full h-full' alt={props.content.headline}/>}

                                <p className='mt-4 text-3xl text-black'>
                                    {date.toLocaleString('de-DE', {
                                        year: 'numeric', 
                                        month: 'long', 
                                        day: 'numeric'
                                    })}
                                </p>
                                
                                <p className='mt-2 text-base text-black'>
                                    {date.toLocaleString('de-DE', {
                                        year: 'numeric', 
                                        month: 'long', 
                                        day: 'numeric'
                                    })}
                                </p>
                            </div>
                        </div>
                    </div>
                )

            case 'mp4':
                return (
                    <div className="relative z-20 h-full shadow-lg">
                        <div className="relative flex flex-wrap items-center justify-center h-full">
                            <div className="relative w-full h-auto p-0 overflow-hidden bg-white">
                                <Player
                                    playsInline
                                    autoPlay
                                    muted
                                    src={props.content.link.value}
                                />

                                {/* <p className='mt-4 text-3xl text-black'>{props.content.date.value}</p>
                                <p className='mt-2 text-base text-black'>{props.content.date.value}</p> */}
                            </div>
                        </div>
                    </div>
                )

            case 'pdf':
                return (
                    <div className="relative z-20 h-full shadow-lg">
                        <div className="relative flex flex-wrap items-center justify-center h-full">
                            <div className="relative w-auto h-auto p-4 overflow-hidden bg-white">
                                <Document 
                                    className={'h-full m-auto flex justify-center'}
                                    file={props.content.link.value}
                                    options={{
                                        standardFontDataUrl: `https://unpkg.com/pdfjs-dist@${pdfjs.version}/standard_fonts`,
                                        // renderMode: 'none'
                                    }} 
                                    onLoadSuccess={onDocumentLoadSuccess} 
                                >
                                    <Page 
                                        pageNumber={pageNumber} 
                                        renderTextLayer={false}
                                        renderAnnotationLayer={false}
                                        // canvasBackground={'transparent'}
                                        className={'inline-block m-auto object-contain'}
                                        options={{
                                            // renderMode: 'none'
                                        }} 
                                    />
                                </Document>
                                
                                <p className="text-base text-center text-black uppercase">{pageNumber} / {numPages}</p>

                                {/* <p className='mt-4 text-3xl text-black'>{props.content.date.value}</p> */}
                                {/* <p className='mt-2 text-base text-black'><a href={samplePdf}>test</a></p> */}
                            </div>
                        </div>
                    </div>
                )

            default:
        }
        
    }
}