import { useState, useEffect, useContext, useRef } from 'react';
import { useParams, useNavigate, useSearchParams, useLocation } from 'react-router-dom';
import axios from "axios";
import XMLParser from 'react-xml-parser';
import Masonry from 'react-masonry-component';
import BackButton from './BackButton';
import { MediaItem } from './MediaItem';
import { LanguageContext } from '../LanguageProvider';
import { useInstitute } from '../ContentProvider';
import Loading from './Loading';

const masonryOptions = {
  transitionDuration: 0,
  percentPosition: true,
  gutter: 10
}

export function Media() {
  const lang = useContext(LanguageContext)
  const {instituteData, isLoading, setLoading} = useInstitute()
  const {slug} = useParams()
  const {state} = useLocation()
  const [activeFilter, setActiveFilter] = useState(null)
  const [media, setMedia] = useState([])
  const [mediaData, setMediaData] = useState([])
  const [filteredMediaData, setFilteredMediaData] = useState([])

  const all = useRef(null)
  const catRefs = useRef([all])

  const categories = [
    {
      name: {
        de: 'Fotos',
        en: 'Pictures'
      }
    },
    {
      name: {
        de: 'Videos',
        en: 'Videos'
      },
    },
    {
      name: {
        de: 'Präsentationen',
        en: 'Presentations'
      },
    },
    {
      name: {
        de: 'Abschlussarbeiten',
        en: 'Thesis'
      }
    }
  ]

  function slugify(str) {
    str = str.replace(/^\s+|\s+$/g, '')
    str = str.toLowerCase()
    str = str.replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-')
    return str
  }

  const setCategory = path => {
    if (path.includes('Abschlussarbeiten')) {
      return categories[3].name[lang.language] 

    } else if (path.includes('Fotos')) {
      return categories[0].name[lang.language] 

    } else if (path.includes('Praesentationen')) {
      return categories[2].name[lang.language] 

    } else if (path.includes('Videos')) {
      return categories[1].name[lang.language] 

    } else {
      return ''
    }
  }

  const setTitle = obj => {
    const title = {
      de: obj.find(obj => obj.attributes.name === 'Title_de_prop'),
      en: obj.find(obj => obj.attributes.name === 'Title_en_prop'),
      fb: obj.find(obj => obj.attributes.name === 'Title_prop')
    }
    console.log(title)
    return title
  }
  
  const handleFilter = filter => {
    // console.log('filter', filter)
    setLoading(true)
    setActiveFilter(activeFilter === filter ? null : filter)
  }
  
  useEffect(() => {
    console.log('state', state)
    setActiveFilter(state?.filter ? slugify(state.filter) : null)
  }, [state])

  useEffect(() => {
    const getData = async () => {
      const data = await axios
        .get('/json/media.xml')
        .then((res) => {
          const jsonDataFromXml = new XMLParser().parseFromString(res.data)
          setMedia(jsonDataFromXml.getElementsByTagName('doc'))
          setLoading(false)
        })
        .catch((err) => console.log(err))
    }
    getData()
  }, [slug])

  useEffect(() => {
    // console.log('media', media)
    // const validCategories = ['Fotos', 'Videos', 'Präsentationen', 'Abschlussarbeiten']
    const mediaItems = media?.filter(mediaObj => {
      const obj = Object.values(mediaObj.children)
      const dateNow = Date.now()
      const dateReleasedObj = obj.find((obj) => obj.attributes.name === 'released')
      const dateReleased = new Date(dateReleasedObj.value)
      const dateReleasedTS = dateReleased.getTime()
      const dateExpiredObj = obj.find((obj) => obj.attributes.name === 'expired')
      const dateExpired = new Date(dateExpiredObj.value)
      const dateExpiredTS = dateExpired.getTime()
      if (dateNow >= dateReleasedTS && dateNow <= dateExpiredTS) {
        return mediaObj 
      }
    }).map (mediaItem => {
      const obj = Object.values(mediaItem.children)
      
      mediaItem.baseUrl = `https://www.${instituteData.shortname}.uni-stuttgart.de/foyer/` //${lang.language}/
      mediaItem.type = obj.find(obj => obj.attributes.name === 'type')
      mediaItem.suffix = obj.find(obj => obj.attributes.name === 'suffix')
      mediaItem.filename = obj.find(obj => obj.attributes.name === 'filename')
      mediaItem.title = setTitle(obj) // obj.find(obj => obj.attributes.name === 'Title_de_prop')
      mediaItem.date = obj.find(obj => obj.attributes.name === 'timestamp')
      mediaItem.path = obj.find(obj => obj.attributes.name === 'path')
      mediaItem.link = obj.find(obj => obj.attributes.name === 'link')
      mediaItem.folders = obj.find(obj => obj.attributes.name === 'parent-folders')
      mediaItem.category = setCategory(mediaItem.path.value.split('/')) // mediaItem.path.value.includes('Fotos') && itemFolder !== 'Fotos' ? 'Fotos' : itemFolder      

      // const foldername = mediaItem.folders.children[mediaItem.folders.children.length-1].value
      // // if (validCategories.includes(foldername)) {
      // //   console.log('foldername', mediaItem.folders.children[mediaItem.folders.children.length].value)
      // // }
      // mediaItem.filepath = foldername.slice(0, -1)
      // const itemFolder = mediaItem.filepath.split('/').pop()
      // console.log('itemFolder', itemFolder)
      
      return mediaItem
    })
    setMediaData(mediaItems)
  }, [media])

  useEffect(() => {
    // console.log('activeFilter', activeFilter, 'mediaData', mediaData)
    setFilteredMediaData(activeFilter === null ? mediaData : mediaData.filter(item => slugify(item.category) === activeFilter))
    setLoading(false)
  }, [mediaData, activeFilter])
  
  useEffect(() => {
    // console.log('filteredMediaData', filteredMediaData)
  }, [filteredMediaData])
  
  return (
    <main className="relative flex items-center w-screen h-screen overflow-hidden">
      <div className="container max-w-[1440px] h-[calc(100%-8rem)] mx-auto flex flex-wrap justify-center">
        <div className="relative h-[calc(100%-86px)] w-full overflow-hidden"> 
          <div className="container max-w-[1280px] h-full mx-auto overflow-y-scroll">
            {isLoading && <Loading/>}

            {filteredMediaData && 
              <Masonry
                className={'relative media-grid w-full h-full'}
                options={masonryOptions}
                disableImagesLoaded={false}
                updateOnEachImageLoad={false}
              >
                {filteredMediaData?.map((item, index) => <MediaItem key={index} content={item} />)}
              </Masonry>}
          </div>
        </div>

        <div className="relative self-end w-full">
          <BackButton/>

          <div className="container max-w-[1280px] h-full mx-auto flex">
            <div className="w-1/2 py-4 bg-white text-blue">
              <p ref={all} className="block text-3xl text-center uppercase" onClick={() => handleFilter(null, all)}>
                {lang.language === 'de' ? 'Mediathek' : 'Media'}
              </p>
            </div>

            <div className="grid w-1/2 h-full grid-cols-2 gap-2 pl-4">
              {categories?.map((cat, index) => {
                  
                  const catName = cat.name[lang.language]
                  const catSlug = slugify(catName)

                  return (
                    <div 
                      key={index} 
                      ref={el => catRefs.current[index] = el} 
                      id={catSlug}
                      className={`border-2 border-white ${catSlug === activeFilter ? 'text-blue bg-white' : 'text-white'}`} 
                      onClick={() => handleFilter(catSlug)}
                    >
                      <span className='block text-[18px] uppercase text-center'>
                        {catName}
                      </span>
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}
