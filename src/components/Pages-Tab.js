// import { useState, useEffect } from 'react';

import { Tab } from '@headlessui/react'
import ArrowButton from './ArrowButton';
import SubNavigation from './SubNavigation'; 

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export function Pages(props) {
    const pages = props.content;

    const listTabs = pages.map((page, index) =>
        <Tab
            key={index}
            className={({ selected }) => classNames("block my-2 text-2xl text-white uppercase text-left", selected ? " active" : null)}
        >
            {({ selected }) => (
                <>
                    <div className='flex flex-nowrap items-center relative'>
                        <ArrowButton size={'w-6 h-6'} color={selected ? 'none' : 'white'} bg={selected ? 'white' : 'transparent'} rotation={page.projects.length === 0 && selected ? '-rotate-90' : '-rotate-90'} /> 
                        <span className='mx-2'>{page.name}</span>
                    </div>

                    {selected ? <SubNavigation projects={page.projects} /> : null}
                </>
            )}
        </Tab>
    )
    
    const listPanels = pages.map((page, index) =>
        <Tab.Panel
            key={index}
            className={({ selected }) => classNames("block my-2 w-full h-full bg-transparent text-sm", selected ? "" : "")}
        >
            <div className='w-full h-full overflow-auto'>
                <h2 className='text-2xl text-white font-bold mb-2'>{page.name}</h2>

                <iframe
                    className='w-full h-[calc(100%-80px)]'
                    title={page.title}
                    src={page.url}
                    width="100%"
                    height="100%"
                ></iframe>

                {/* {page.content.map((el, index) =>
                    <div className='' key={index}>
                        <h3 className='text-2xl text-white mb-2'>{el.title}</h3>
                        <p>{el.text ? el.text : null}</p>        
                    </div>
                )} */}
            </div>
        </Tab.Panel>
    )

    const listSubPanels = pages.map((page, index) =>
        page.projects.map((subpage, index) =>
            <Tab.Panel
                key={index}
                className={({ selected }) => classNames("block my-2 w-full h-full bg-transparent text-sm", selected ? "" : "")}
            >
                <div className='w-full h-full overflow-auto'>
                    <h2 className='text-2xl text-white font-bold mb-2'>{subpage.name}</h2>

                    <iframe
                        className='w-full h-[calc(100%-80px)]'
                        title={subpage.title}
                        src={subpage.url}
                        width="100%"
                        height="100%"
                    ></iframe>
                </div>
            </Tab.Panel>
        )
    )
    

    return (
        <div className='flex flex-nowrap h-full border-white border-2 p-6'>
            <Tab.Group vertical>
                <Tab.List className="flex justify-end flex-col space-x-1 w-1/2 h-full w-1/2">
                    {listTabs} 
                </Tab.List>

                <Tab.Panels className="w-1/2 h-full">
                    {listPanels}
                    <div className='test'>
                        {listSubPanels}
                    </div>
                </Tab.Panels>
            </Tab.Group>
        </div>
    )
        
}