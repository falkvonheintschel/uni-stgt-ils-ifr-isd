import React, { useContext } from "react";
import { LanguageContext } from '../LanguageProvider';

const LanguageSelector = () => {
  const { language, updateLanguage } = useContext(LanguageContext)

	const handleUpdateLanguage = ({ target: { value } }) => {
    // console.log('new language', value)
		updateLanguage(value)
	}

  return (
    <div className='overflow-hidden w-22 h-36'> 
      <div value={language} className={`flex items-center overflow-hidden w-16 h-16 mb-4 text-xl ${language === 'de' ? 'text-blue bg-white' : 'text-white bg-transparent'} border-2 border-white rounded-full`}>
        <label className={`block w-full cursor-pointer select-none rounded-xl text-center ${language === 'de' ? '' : ``}`} htmlFor="lang-de">
          DE
          <input type="radio" value="de" name="language" id="lang-de" className='hidden' onChange={handleUpdateLanguage} />
        </label>
      </div>
      
      <div value={language} className={`flex items-center overflow-hidden w-16 h-16 mb-4 text-xl ${language === 'en' ? 'text-blue bg-white' : 'text-white bg-transparent'} border-2 border-white rounded-full`}>
        <label className={`block w-full cursor-pointer select-none rounded-xl text-center ${language === 'en' ? '' : ``}`} htmlFor="lang-en">
          EN
          <input type="radio" value="en" name="language" id="lang-en" className='hidden' onChange={handleUpdateLanguage}/>
        </label>
      </div>
    </div>
  )
}

export default LanguageSelector