// import { useState, useEffect, useRef, useContext } from 'react';
// import { LanguageContext } from "../LanguageProvider"
// import axios from "axios";

export function HomeInfo({infoModal, handleClose}) {
    console.log('infoModal', infoModal)
    return (
        <div className="relative w-full h-full z-10 flex items-center" onClick={handleClose}>
            <div className={`relative container max-w-[1280px] mx-auto w-full h-1/2 z-10 self-center flex flex-wrap items-center ${infoModal.length > 1 ? 'text-6xl' : 'text-9xl'}`}>
                {infoModal.map((infoTxt, i) => {
                    // const values = Object.values(obj.infoTxt.children)
                    // const dateReleasedObj = values.find((obj) => obj.attributes.name === 'released')
                    // const dateReleased = new Date(dateReleasedObj.value)
                    
                    return(
                        <div key={i} className='w-full'>
                            <h1 className="relative leading-none z-10 uppercase">{infoTxt.children[0]?.value}</h1>
                        </div>
                    )
                }
                )}
            </div>
        </div>
    )
}