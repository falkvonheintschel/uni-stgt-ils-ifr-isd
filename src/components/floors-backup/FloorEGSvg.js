import { useEffect, useRef } from "react";

export function FloorEGSvg({room, handleRoom}) {

    const svg = useRef(null);

    useEffect(() => {
        if (svg.current == null) 
        return

        svg.current.querySelectorAll('text').forEach(el => {
            el.style.fill = 'white'
        })

        if (room == null)
        return

        let group = svg.current.getElementById(`${room}`)
        if (group == null) 
        return
        group.querySelector('text').style.fill = 'red'

    }, [room, svg])
    
    return (
        <svg 
            xmlns="http://www.w3.org/2000/svg" 
            viewBox="0 0 481.9 822"
            width="100%" //481.9px
            height="100%" //822px
            ref={svg}
        >
            <g id="Grundriss_EG" data-name="Grundriss EG">
                <polyline points="130.2 385.1 130.2 412.7 258.9 412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="258.9 610.5 130.1 610.5 130.1 642.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="158.9" y1="627.5" x2="130.1" y2="627.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="202.4" y1="627.5" x2="158.9" y2="627.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="258.9" y1="627.5" x2="202.4" y2="627.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="249.9" y1="610.5" x2="249.9" y2="627.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="251.9" y1="609.7" x2="251.9" y2="627.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.6px' }} />
                <line x1="297.3" y1="627.5" x2="284.9" y2="627.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }} />
                <line x1="455.7" y1="745.6" x2="467.4" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="349.2" y1="745.6" x2="440.2" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                <line x1="350.6" y1="697.7" x2="350.6" y2="708.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="353" y1="184.2" x2="353" y2="205.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="353" y1="75.2" x2="353" y2="205.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="353" y1="516.3" x2="353" y2="573.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="350.9" y1="516.3" x2="350.9" y2="574.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="353" y1="371.2" x2="353" y2="470.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="350.9" y1="371.2" x2="350.9" y2="470.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="249.9" y1="367.6" x2="249.9" y2="412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.5" y1="485.2" x2="102.5" y2="561.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="100.4" y1="524.1" x2="100.4" y2="561.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.5" y1="75.2" x2="102.5" y2="114.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="100.4" y1="75.2" x2="100.4" y2="114.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="128.5" y1="71.6" x2="128.5" y2="62.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }} />
                <polyline points="297.5 92.1 297.5 107.3 126.8 107.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                <line x1="419.3" y1="745.6" x2="419.3" y2="802.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="417.3" y1="744.8" x2="417.3" y2="804.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="297.5" y1="367.6" x2="285.9" y2="367.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="295.4" y1="365.5" x2="285.9" y2="365.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="258.9" y1="367.6" x2="251.4" y2="367.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="258.9" y1="414.8" x2="251.4" y2="414.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="251.9 365.5 251.9 414.8 128.2 414.8 128.2 385.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="285.9 414.8 297.5 414.8 297.5 143.4 128.2 143.4 128.2 367.6 251.9 367.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="258.9 365.5 130.2 365.5 130.2 145.4 295.4 145.4 295.4 412.7 285.9 412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="466.8" y1="186.6" x2="350.9" y2="186.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                <polyline points="102.5 706.7 102.5 745.7 15.9 745.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.9" y1="688.3" x2="13.9" y2="688.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="15.9 642.5 100.4 642.5 100.4 671.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="13.9 640.5 102.5 640.5 102.5 671.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="15.9 579.6 100.4 579.6 100.4 624.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="13.9 577.5 102.5 577.5 102.5 624.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.5" y1="524.1" x2="15.9" y2="524.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="100.4" y1="522.1" x2="13.9" y2="522.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="100.4" y1="485.2" x2="100.4" y2="525.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 429.8 102.5 468.9 15.9 468.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 429.8 100.4 466.8 13.9 466.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 371.2 102.5 411.7 15.9 411.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 371.2 100.4 409.7 13.9 409.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 315.8 102.5 354.8 15.9 354.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 315.8 100.4 352.8 13.9 352.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 260.4 102.5 299.5 15.9 299.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 260.4 100.4 297.5 13.9 297.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 205 102.5 244.1 15.9 244.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 205 100.4 242.1 13.9 242.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.9" y1="188.7" x2="15.9" y2="188.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="102.9" y1="186.6" x2="13.9" y2="186.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="15.9 133.3 100.4 133.3 100.4 168.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="13.9 131.2 102.9 131.2 103 168.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="62.5" y1="77.9" x2="15.9" y2="77.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="60.5" y1="75.8" x2="13.9" y2="75.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 101.2 60.5 101.2 60.5 52.6 102.5 52.6 102.5 59.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 99.1 62.5 99.1 62.5 54.6 100.4 54.6 100.4 59.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="128.2 41.6 128.2 16.5 297.5 16.5 297.5 76.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="350.9 414.9 467.4 414.9 467.4 16.5 350.9 16.5 350.9 59.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="466.8 297.3 350.9 297.3 350.9 336.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                <line x1="350.9" y1="221" x2="350.9" y2="281" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="350.9" y1="75.2" x2="350.9" y2="205.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="350.9" y1="572.7" x2="350.9" y2="673.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="466.8" y1="572.7" x2="350.9" y2="572.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="128.1 661.3 128.1 804.5 297.3 804.5 297.3 608.5 285.9 608.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="100.4 762.1 100.4 802.5 15.9 802.5 15.9 18.5 100.4 18.5 100.4 37.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="102.5 762.1 102.5 804.5 13.9 804.5 13.9 16.5 102.5 16.5 102.5 37.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <g>
                <line x1="366.7" y1="771" x2="366.7" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="362.9" y1="771" x2="362.9" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="370.4" y1="771" x2="370.4" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="374.2" y1="771" x2="374.2" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="378" y1="771" x2="378" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="381.8" y1="771" x2="381.8" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="385.6" y1="771" x2="385.6" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="389.4" y1="771" x2="389.4" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="366.7" y1="802.5" x2="366.7" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="362.9" y1="802.5" x2="362.9" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="359.1" y1="802.5" x2="359.1" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="370.4" y1="802.5" x2="370.4" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="374.2" y1="802.5" x2="374.2" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="378" y1="802.5" x2="378" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="381.8" y1="802.5" x2="381.8" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="385.6" y1="802.5" x2="385.6" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="389.4" y1="802.5" x2="389.4" y2="773" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="393.2" y1="771" x2="393.2" y2="745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                </g>
                <polyline points="127 70.2 153.9 70.2 153.9 105.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="349.2 708.2 376.5 708.2 376.5 745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                <polyline points="397.7 771 353.3 771 353.3 804.5 467.4 804.5 467.4 571.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <g id="Treppe">
                <g>
                    <g>
                    <g>
                        <line x1="193.1" y1="553.5" x2="193.1" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="189.3" y1="553.5" x2="189.3" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="196.8" y1="553.5" x2="196.8" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="200.6" y1="553.5" x2="200.6" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="204.4" y1="553.5" x2="204.4" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="208.2" y1="553.5" x2="208.2" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="212" y1="553.5" x2="212" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="215.8" y1="553.5" x2="215.8" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="219.5" y1="553.5" x2="219.5" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    </g>
                    <g>
                        <line x1="159" y1="553.5" x2="159" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="155.2" y1="553.5" x2="155.2" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="162.8" y1="553.5" x2="162.8" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="147.6" y1="553.5" x2="147.6" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="151.4" y1="553.5" x2="151.4" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="166.6" y1="553.5" x2="166.6" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="170.3" y1="553.5" x2="170.3" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="174.1" y1="553.5" x2="174.1" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="177.9" y1="553.5" x2="177.9" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="181.7" y1="553.5" x2="181.7" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                        <line x1="185.5" y1="553.5" x2="185.5" y2="531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    </g>
                    </g>
                    <g>
                    <polyline points="224.1 554.7 141.6 554.7 141.6 554.7 141.6 530.4 224.1 530.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <polyline points="224.1 553.5 143.1 553.5 143.1 553.5 143.1 531.4 224.1 531.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    </g>
                </g>
                </g>
                <g id="Treppe-2" data-name="Treppe">
                <line x1="221.6" y1="143.4" x2="221.6" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="217.8" y1="143.4" x2="217.8" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="225.4" y1="143.4" x2="225.4" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="229.2" y1="143.4" x2="229.2" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="232.9" y1="143.4" x2="232.9" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="236.7" y1="143.4" x2="236.7" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="240.5" y1="143.4" x2="240.5" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="249.3" y1="126.3" x2="179.2" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="244.3" y1="143.4" x2="244.3" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <line x1="249.3" y1="125.3" x2="179.2" y2="125.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                <g>
                    <line x1="187.5" y1="143.4" x2="187.5" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="183.8" y1="143.4" x2="183.8" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="191.3" y1="143.4" x2="191.3" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="195.1" y1="143.4" x2="195.1" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="198.9" y1="143.4" x2="198.9" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="202.7" y1="143.4" x2="202.7" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="206.5" y1="143.4" x2="206.5" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="210.2" y1="143.4" x2="210.2" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                    <line x1="214" y1="143.4" x2="214" y2="126.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                </g>
                </g>
                <path id="Toilette_D" data-name="Toilette D" d="M57.3,104.1v-23.1H18.9v23.1c.1,0,.1,4.5,.1,4.5v19.6H97v-24.1H57.3Zm-19.8,18.5c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-8h0v-1.1c0-.2-.2-.4-.4-.4h0s-2.4,0-2.4,0c-.3,0-.6,0-.8-.1h0s0,0-.1,0c0,0,0,0,0,0-1.3-.6-2-2-1.5-3.4l3-7.8c.6-1.7,1.3-3.6,1.8-4.8,.2-.6,.9-.9,1.5-.7h0c.6,.2,.9,.9,.7,1.6l-4.6,11.7-.3,.8c0,.2,0,.4,0,.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0,.2,0h2.3c1.5,0,2.7,1.2,2.7,2.7v9.1Zm.5-27.8c-.4,0-.7,0-1-.2-.3-.1-.6-.3-.8-.5-.2-.2-.4-.5-.5-.8-.1-.3-.2-.6-.2-1v-3c0-.3,0-.7,.2-1,.1-.3,.3-.5,.5-.7h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.5-.4,.8-.5,.3-.1,.7-.2,1-.2s.7,0,1,.2c.3,.1,.5,.3,.8,.5h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.4,.5,.5,.8,.1,.3,.2,.6,.2,1v3c0,.3,0,.7-.2,1-.1,.3-.3,.6-.5,.8-.2,.2-.5,.4-.8,.5-.3,.1-.7,.2-1,.2h0Zm6.5,18s0,0,0,0c0,0,0,0-.1,0h0c-.2,0-.5,.1-.8,.1h-2.4c-.2,0-.4,.2-.4,.4v1.1h0v8c0,.6-.5,1.1-1.1,1.1s-1.1-.5-1.1-1.1v-9.1c0-1.5,1.2-2.7,2.7-2.7h0s2.3,0,2.3,0c0,0,.1,0,.2,0,0,0,0,0,0,0,0,0,0,0,0,0,.1-.1,.2-.3,0-.5l-.3-.8-4.6-11.7c-.2-.6,0-1.3,.7-1.6h0c.6-.2,1.3,0,1.5,.7,.4,1.2,1.1,3.1,1.8,4.8l3,7.8c.5,1.3-.2,2.8-1.5,3.4Z" style={{ fill: '#fff' }} />
                <path id="Toilette_H" data-name="Toilette H" d="M19.1,21.6v51H57.1v-23.1h40V21.6H19.1Zm18.5,29.6h0v13.2c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-24.2c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v9.9c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-9.9c0-1.5,1.3-2.8,2.9-2.7,1.4,0,2.5,1.3,2.5,2.8v10.9Zm.4-14.5c-.4,0-.7,0-1-.2-.3-.1-.6-.3-.8-.5-.2-.2-.4-.5-.5-.8-.1-.3-.2-.6-.2-1v-3c0-.3,0-.7,.2-1,.1-.3,.3-.5,.5-.7h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.5-.4,.8-.5,.3-.1,.7-.2,1-.2s.7,0,1,.2c.3,.1,.5,.3,.8,.5h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.4,.5,.5,.8,.1,.3,.2,.6,.2,1v3c0,.3,0,.7-.2,1-.1,.3-.3,.6-.5,.8-.2,.2-.5,.4-.8,.5-.3,.1-.7,.2-1,.2h0Zm5.8,13.4c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-9.9c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v24.2c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-24.3c0-1.5,1.3-2.8,2.9-2.7,1.4,0,2.5,1.3,2.5,2.8v9.8Z" style={{ fill: '#fff' }} />
                <g id="Toilette_B" data-name="Toilette B">
                <path d="M65.6,57.4v38.8h31.6V57.4h-31.6Zm12.3,7.1c0-.4,.2-.8,.5-1.1,0,0,0,0,0,0,0,0,.1-.2,.2-.2h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.4,.7-.5,.3-.1,.6-.2,1-.2s.7,0,1,.2c.3,.1,.5,.3,.7,.5h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.5,.5,.7,.1,.3,.2,.6,.2,.9v2.9c0,.3,0,.6-.2,.9-.1,.3-.3,.5-.5,.8-.2,.2-.5,.4-.8,.5-.3,.1-.6,.2-1,.2h0c-.4,0-.7,0-1-.2-.3-.1-.6-.3-.8-.5-.2-.2-.4-.5-.5-.8-.1-.3-.2-.6-.2-.9h0c0-.1,0-.3,0-.4v-2.7Zm9.6,21.4c-.8,3.7-4.2,6.5-8.2,6.5s-8.4-3.7-8.4-8.4,2.5-6.9,5.9-8c.7-.2,1.4,.3,1.4,1h0c0,.5-.3,.9-.7,1-2.6,.8-4.5,3.3-4.4,6.3,.2,3.1,2.7,5.7,5.8,5.9,3.2,.2,5.9-1.9,6.6-4.8,.1-.5,.5-.8,1-.8h0c.7,0,1.2,.6,1,1.3Zm3.8,4.1c0,.6-.5,1.1-1.1,1.1h0c-.6,0-1.1-.5-1.1-1.1v-4.7c0-.5-.2-1.7-1.7-1.7h-1.5s-2.7,0-2.7,0c-2.8,0-3.9-2.3-3.9-3.9v-6.8c0-.6,.5-1.1,1.1-1.1h0c.6,0,1.1,.5,1.1,1.1v6.8c0,.5,.2,1.7,1.7,1.7h1.5s2.7,0,2.7,0c2.8,0,3.9,2.3,3.9,3.9v4.7Z" style={{ fill: '#fff' }} />
                </g>
                <g id="Eingang">
                <path d="M355.3,493.4l14.4-14.6v28.7l-14.4-14Z" style={{ fill: 'none' }} />
                <path d="M358.1,493.4l9.5,9.3v-19l-9.5,9.7m-2.8,0l14.4-14.6v28.7l-14.4-14Z" style={{ fill: '#fff' }} />
                </g>
                <g id="Eingang-2" data-name="Eingang">
                <path d="M393.8,497.7h-5.8c-.5,0-.9-.4-.9-.9v-8.9c0-.4,.4-.8,.9-.8h5.8c.5,0,.9,.4,.9,.8s-.3,.9-.9,.9h-4.9v2.4h3.3c.5,0,.8,.4,.8,.9s-.4,.9-.8,.9h-3.3v3.2h4.9c.5,0,.9,.4,.9,.9s-.4,.9-.9,.9Z" style={{ fill: '#fff' }} />
                <path d="M398.8,497.7h-1.7c-.6,0-.9-.5-.9-.9s.4-.9,.9-.9v-7.2c-.6,0-.9-.4-.9-.9s.3-.8,.9-.8h1.7c.6,0,.9,.4,.9,.8s-.4,.9-.9,.9v7.2c.5,0,.9,.4,.9,.9s-.3,.9-.9,.9Z" style={{ fill: '#fff' }} />
                <path d="M408.5,497.7c-.3,0-.5-.1-.7-.4l-4.2-6.5v6.1c0,.6-.4,.9-.9,.9s-.9-.3-.9-.9v-8.9c0-.5,.4-.8,.9-.8s.5,.1,.7,.4l4.2,6.5v-6.1c0-.6,.4-.8,.9-.8s.9,.3,.9,.8v8.9c0,.5-.4,.9-.9,.9Z" style={{ fill: '#fff' }} />
                <path d="M416.2,497.9c-3.3,0-4.4-2.8-4.4-5.6s1.2-5.6,4.3-5.6,2.5,.5,3.3,1.6c.1,.2,.2,.3,.2,.5,0,.5-.4,.9-.9,.9s-.5-.1-.7-.4c-.4-.5-.9-.9-1.9-.9-2.3,0-2.6,2.1-2.6,3.9s.4,3.9,2.6,3.9,2.3-.9,2.6-2.6h-.9c-.6,0-.9-.4-.9-.9s.3-.9,.9-.9h1.9c.5,0,.8,.3,.8,.8h0c0,2.8-1.3,5.2-4.3,5.2Z" style={{ fill: '#fff' }} />
                <path d="M430.2,497.7c-.3,0-.6-.2-.8-.5l-.6-1.6h-4.6l-.7,1.6c-.1,.4-.4,.5-.7,.5s-.9-.4-.9-.9,0-.2,0-.3l3.6-8.9c.2-.4,.5-.7,.9-.7s.7,.2,.9,.7l3.6,8.9c0,.1,0,.2,0,.4,0,.5-.5,.8-.9,.8Zm-3.7-7.6l-1.6,3.8h3.1l-1.6-3.8Z" style={{ fill: '#fff' }} />
                <path d="M439.5,497.7c-.3,0-.5-.1-.7-.4l-4.2-6.5v6.1c0,.6-.4,.9-.9,.9s-.9-.3-.9-.9v-8.9c0-.5,.4-.8,.9-.8s.5,.1,.7,.4l4.2,6.5v-6.1c0-.6,.4-.8,.9-.8s.9,.3,.9,.8v8.9c0,.5-.4,.9-.9,.9Z" style={{ fill: '#fff' }} />
                <path d="M447.2,497.9c-3.3,0-4.4-2.8-4.4-5.6s1.2-5.6,4.3-5.6,2.5,.5,3.3,1.6c.1,.2,.2,.3,.2,.5,0,.5-.4,.9-.9,.9s-.5-.1-.7-.4c-.4-.5-.9-.9-1.9-.9-2.3,0-2.6,2.1-2.6,3.9s.4,3.9,2.6,3.9,2.3-.9,2.6-2.6h-.9c-.6,0-.9-.4-.9-.9s.3-.9,.9-.9h1.9c.5,0,.8,.3,.8,.8h0c0,2.8-1.3,5.2-4.3,5.2Z" style={{ fill: '#fff' }} />
                </g>
                <g id="Standort">
                <g id="Gruppe_1000" data-name="Gruppe 1000">
                    <path id="Pfad_990" data-name="Pfad 990" d="M195.1,430.2c7.3,0,13.2,5.9,13.2,13.2s-5.9,13.2-13.2,13.2-13.2-5.9-13.2-13.2c0-7.3,5.9-13.2,13.2-13.2h0Z" style={{ fill: '#fff' }} />
                    <path id="Pfad_769" data-name="Pfad 769" d="M195.1,437.6c3.2,0,5.8,2.6,5.8,5.8s-2.6,5.8-5.8,5.8-5.8-2.6-5.8-5.8c0-3.2,2.6-5.8,5.8-5.8h0Z" style={{ fill: '#036496' }} />
                </g>
                </g>
                <g id="Aufzug">
                <polygon points="362.3 713.3 370.9 713.3 370.9 738.6 362.3 738.6 362.3 740.7 373.4 740.7 373.4 711.1 362.3 711.1 362.3 713.3" style={{ fill: '#fff' }} />
                <rect x="353.7" y="715.3" width="6.6" height="21.4" style={{ fill: '#fff' }} />
                <polygon points="360.3 738.6 351.7 738.6 351.7 713.3 360.3 713.3 360.3 711.1 349.2 711.1 349.2 740.7 360.3 740.7 360.3 738.6" style={{ fill: '#fff' }} />
                <rect x="362.3" y="715.3" width="6.6" height="21.4" style={{ fill: '#fff' }} />
                </g>
                <g id="Aufzug-2" data-name="Aufzug">
                <polygon points="140.2 75.1 148.7 75.1 148.7 100.4 140.2 100.4 140.2 102.5 151.3 102.5 151.3 72.9 140.2 72.9 140.2 75.1" style={{ fill: '#fff' }} />
                <rect x="131.5" y="77.1" width="6.6" height="21.4" style={{ fill: '#fff' }} />
                <polygon points="138.2 100.4 129.5 100.4 129.5 75.1 138.2 75.1 138.2 72.9 127 72.9 127 102.5 138.2 102.5 138.2 100.4" style={{ fill: '#fff' }} />
                <rect x="140.2" y="77.1" width="6.6" height="21.4" style={{ fill: '#fff' }} />
                </g>
                <polyline points="397.7 773 355.3 773 355.3 802.5 465.4 802.5 465.4 572.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <polyline points="258.9 608.5 128.1 608.5 128.1 642.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="465.3" y1="244" x2="352" y2="244" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                <line x1="466.3" y1="354.8" x2="350.4" y2="354.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
            </g>

            <g id="Räume_EG" data-name="Räume EG">
                <g id="V27.03" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="V27.03" transform="translate(384 105.9)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>V</tspan><tspan x="10.8" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan><tspan x="26.8" y="0">.0</tspan><tspan x="41.6" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
                    <g id="Raum">
                        <polyline points="464.9 187.6 464.9 18.3 352.5 18.3 352.5 59.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                        <g>
                        <line x1="465.7" y1="186.3" x2="352.8" y2="186.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                        <polyline points="465.7 18.3 353.3 18.3 353.3 59.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                        <line x1="353.3" y1="75" x2="353.3" y2="188" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                        </g>
                    </g>
                </g>
                <g id="0.004" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <rect x="509.3" y="251.4" width="105.9" height="48.1" style={{ fill: 'none' }} />
                    <text id="_0.004" data-name="0.004" transform="translate(388.6 219.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.00</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
                    <line x1="353" y1="221" x2="353" y2="242" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="353.3" y1="183.8" x2="353.3" y2="204.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="465.4" y1="242" x2="351.6" y2="242" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
                    <line x1="466.4" y1="188.6" x2="353" y2="188.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="464.9" y1="188.4" x2="464.9" y2="242.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.005" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.005" data-name="0.005" transform="translate(388.3 276.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.0</tspan><tspan x="23.6" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="32.7" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
                    <line x1="464.9" y1="241.8" x2="464.9" y2="297.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="353" y1="242.8" x2="353" y2="281" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="465.3" y1="244" x2="352" y2="244" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="466.3" y1="297.3" x2="350.8" y2="297.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.006" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.006" data-name="0.006" transform="translate(388.1 332.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.00</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
                    <line x1="464.6" y1="298.1" x2="464.6" y2="354.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="465.8" y1="352.8" x2="350.4" y2="352.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="466 299.4 352.2 299.4 352.2 336.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.007" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.007" data-name="0.007" transform="translate(388.8 388.5)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.00</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan></text>
                    <line x1="464.9" y1="355.9" x2="464.9" y2="414.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="353" y1="412.8" x2="466" y2="412.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="466.3" y1="354.8" x2="350.4" y2="354.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="353" y1="371.2" x2="353" y2="414.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
                </g>
                <g id="0.012" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.012" data-name="0.012" transform="translate(391.1 662.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="9.8" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="14.9" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="25.3" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="31.3" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
                    <line x1="455.7" y1="743.6" x2="465.4" y2="743.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="349.2" y1="743.6" x2="440.2" y2="743.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                    <line x1="352.6" y1="697.7" x2="352.6" y2="706.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="466.8 574.7 353 574.7 353 673.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="349.2 707.3 378.5 707.3 378.5 745.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="465.4" y1="573.8" x2="465.4" y2="744.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="V27.01" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="V27.01" transform="translate(188.3 725)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>V</tspan><tspan x="10.4" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan><tspan x="18.6" y="0" style={{ letterSpacing: '-.1em' }}>7</tspan><tspan x="25.1" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="29.8" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="38.9" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan></text>
                    <line x1="158.9" y1="629.6" x2="128.1" y2="629.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="202.4" y1="629.6" x2="158.9" y2="629.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="258.9" y1="629.6" x2="202.4" y2="629.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="130.1 661.3 130.1 802.5 295.3 802.5 295.3 628.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="296.3" y1="629.6" x2="284.9" y2="629.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                </g>
                <g id="0.043" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.043" data-name="0.043" transform="translate(37.1 780)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">0</tspan><tspan x="23.9" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="32" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
                    <line x1="15.9" y1="744" x2="15.9" y2="801.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="15.9" y1="745.4" x2="101.4" y2="745.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="14.6" y1="802.8" x2="102.3" y2="802.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="100.8" y1="762.1" x2="100.8" y2="801.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.044" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.044" data-name="0.044" transform="translate(36.7 723.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">0</tspan><tspan x="23.9" y="0" style={{ letterSpacing: '0.1em' }}>44</tspan></text>
                    <line x1="16.1" y1="689" x2="16.1" y2="744.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="100 706.7 100 743.7 100 743.7 14.6 743.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="102.9" y1="690.3" x2="15.9" y2="690.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.045" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.045" data-name="0.045" transform="translate(36.5 669.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="24.3" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="32.7" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
                    <line x1="16.1" y1="689.6" x2="16.1" y2="640.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="100 671.9 100 641.9 100 641.9 14.7 641.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.5px' }} />
                    <line x1="103" y1="688.2" x2="16" y2="688.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
                <g id="0.046" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.2" y1="642.9" x2="16.2" y2="579.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }} />
                    <polyline points="100 624.1 100 581 100 581 14.7 581" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                    <line x1="101.1" y1="641.1" x2="16" y2="641.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }} />
                    <text id="_0.046" data-name="0.046" transform="translate(36.2 617.1)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="24.3" y="0">4</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
                </g>
                <g id="0.047" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.047" data-name="0.047" transform="translate(36.9 556.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="24.3" y="0">4</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan></text>
                    <line x1="16.1" y1="579.9" x2="16.1" y2="522.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="99.9 561.2 99.9 524.5 99.9 524.5 14.6 524.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="101.5" y1="578.3" x2="15.9" y2="578.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }} />
                </g>
                <g id="0.048" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.3" y1="466.6" x2="16.3" y2="523.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="100.2 485.3 100.2 522.1 100.2 522.1 14.9 522.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="101.7" y1="468.2" x2="16.2" y2="468.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }} />
                    <text id="_0.048" data-name="0.048" transform="translate(36.4 500.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="24.3" y="0">4</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>8</tspan></text>
                </g>
                <g id="0.049" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.5" y1="410.2" x2="16.5" y2="467.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="100.3 429.8 100.3 465.6 100.3 465.6 15 465.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="101.7" y1="411.8" x2="16.3" y2="411.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }} />
                    <text id="_0.049" data-name="0.049" transform="translate(36.2 444.5)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="24.3" y="0">4</tspan><tspan x="33" y="0" style={{ letterSpacing: '0.1em' }}>9</tspan></text>
                </g>
                <g id="0.050" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.5" y1="354.1" x2="16.5" y2="411.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="100.3 373.7 100.3 409.5 100.3 409.5 15 409.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="103.8" y1="355.7" x2="16.3" y2="355.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <text id="_0.050" data-name="0.050" transform="translate(35.9 387)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">05</tspan><tspan x="33.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan></text>
                </g>
                <g id="0.051" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.7" y1="297.7" x2="16.7" y2="354.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="102.4 317.2 102.4 353.1 102.4 353.1 15.2 353.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="104" y1="299.3" x2="16.5" y2="299.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <text id="_0.051" data-name="0.051" transform="translate(38.1 332.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">0</tspan><tspan x="23.9" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="33.3" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan></text>
                </g>
                <g id="0.052" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.7" y1="242.7" x2="16.7" y2="299.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="102.6 262.2 102.6 298.1 102.6 298.1 15.2 298.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
                    <line x1="104.1" y1="244.3" x2="16.5" y2="244.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <text id="_0.052" data-name="0.052" transform="translate(36.8 275.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">05</tspan><tspan x="33.7" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
                </g>
                <g id="0.053" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.8" y1="186.8" x2="16.8" y2="243.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="102.8 205 102.8 241.4 102.8 241.4 15.3 241.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="102.9" y1="188" x2="16.6" y2="188" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.4px' }} />
                    <text id="_0.053" data-name="0.053" transform="translate(37.7 220.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">05</tspan><tspan x="33.7" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
                </g>
                <g id="0.054" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="16.9" y1="187.5" x2="16.9" y2="130.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
                    <polyline points="103 168.1 103 131.8 103 131.8 15.5 131.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <line x1="103" y1="186.3" x2="16.7" y2="186.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.4px' }} />
                    <text id="_0.054" data-name="0.054" transform="translate(36.4 165.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.</tspan><tspan x="14.1" y="0">05</tspan><tspan x="33.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
                </g>
                <g id="0.028" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="_0.028" data-name="0.028" transform="translate(190.1 63.9)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>0.0</tspan><tspan x="23.6" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan><tspan x="31.5" y="0" style={{ letterSpacing: '0.1em' }}>8</tspan></text>
                    <line x1="130.5" y1="67.9" x2="130.5" y2="62.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="295.4 92.1 295.4 105.2 126.8 105.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                    <polyline points="130.2 41.6 130.2 18.5 295.4 18.5 295.4 76.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                    <polyline points="127 68.2 155.9 68.2 155.9 107.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
                </g>
                <g id="V27.02" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text id="V27.02" transform="translate(186.7 260.1)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>V</tspan><tspan x="10.4" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan><tspan x="18.6" y="0" style={{ letterSpacing: '-.1em' }}>7</tspan><tspan x="25.1" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="39.2" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
                    <polyline points="258.9 365.5 130.2 365.5 130.2 145.4 295.4 145.4 295.4 365.7 285.9 365.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
                </g>
            </g>
        </svg>
    )

}