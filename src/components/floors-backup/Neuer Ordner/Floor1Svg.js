import { useEffect, useRef } from "react";

import Floor1  from './floors-svg/Grundriss_1OG.svg'

export function Floor1Svg({room, handleRoom}) {

    const svg = useRef(null);
    
    useEffect(() => {
        // console.log(room)
        if (svg.current == null) 
        return

        svg.current.querySelectorAll('text').forEach(el => {
            el.style.fill = 'white'
        })

        if (room == null)
        return

        let group = svg.current.getElementById(`${room}`)
        if (group == null) 
        return

        group.querySelector('text').style.fill = 'red'
    }, [room, svg])
    
    return (

        <object ref={svg} data={Floor1}></object>

        // <svg 
        //     xmlns="http://www.w3.org/2000/svg" 
        //     viewBox="0 0 481.9 822"
        //     width="100%" //481.9px
        //     height="100%" //822px
        //     ref={svg}
        // >
        //     <g id="Grundriss_1_OG" data-name="Grundriss 1 OG">
        //         <line x1="127.9" y1="607.5" x2="127.9" y2="803.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="202.1 599.5 130 599.5 130 694.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="202.1" y1="597.5" x2="127.9" y2="597.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296" y1="694.1" x2="129.1" y2="694.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296.9" y1="597.5" x2="200.6" y2="597.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296.9" y1="599.5" x2="200.6" y2="599.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="294.4 653.3 185 653.3 185 598.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="296.9 651.2 187.5 651.2 187.5 598.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.5" y1="449.9" x2="350.5" y2="525.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.5" y1="260.1" x2="350.5" y2="298.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="464.6 691.2 350.5 691.2 350.5 652.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="268.9 414.7 211.5 414.7 211.5 403.8 182.5 403.8 182.5 414.9 131 414.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="266.9 414.7 213.5 414.7 213.5 401.8 180.4 401.8 180.4 412.9 128.9 412.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.5" y1="555.4" x2="350.5" y2="575.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="102.2" y1="541.5" x2="102.2" y2="580" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="297.1 101.7 297.1 111.3 127.9 111.3 127.9 107.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="258.5" y1="415.8" x2="251" y2="415.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="252.1 382.5 252.1 415.8 127.9 415.8 127.9 318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="257.3 415.8 297.1 415.8 297.1 382.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="222.9 556.5 297.1 556.5 297.1 415.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="295 413.6 295 554.5 222.9 554.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="464.9" y1="132.2" x2="350.2" y2="132.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 707.6 102.2 746.7 15.8 746.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="100.2 707.6 100.2 744.7 100.2 744.7 13.7 744.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="102.7" y1="691.3" x2="15.8" y2="691.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="13.7 641.4 102.2 641.4 102.2 672.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="82 582.1 100.2 582.1 100.2 617.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="15.8" y1="582.1" x2="66.3" y2="582.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="82 580 102.2 580 102.2 617.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 486.2 102.2 525.1 82 525.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="15.8 469.9 102.2 469.9 102.2 431.1 89.9 431.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="13.7 467.8 100.2 467.8 100.2 433.1 89.9 433.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 372.2 102.2 412.7 15.8 412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 316.8 102.2 355.8 15.8 355.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 261.4 102.2 300.5 15.8 300.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 206 102.2 245.1 15.8 245.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="15.8 189.7 102.2 189.7 102.2 150.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="15.8 134.3 102.2 134.3 102.2 95.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="102.6" y1="78.9" x2="15.8" y2="78.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="127.9 74.8 127.9 17.5 297.1 17.5 297.1 64.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="464.9 298.3 350.5 298.3 350.5 324.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.5" y1="149.3" x2="350.5" y2="213.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="364.8 76.7 350.5 76.7 350.5 101.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.5" y1="573.7" x2="350.5" y2="617.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296.9" y1="671.1" x2="296.9" y2="639.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296.9" y1="752" x2="296.9" y2="692.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="294.9" y1="671.1" x2="294.9" y2="639.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="100.2 763.1 100.2 803.5 15.8 803.5 15.8 19.5 100.2 19.5 100.2 60.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.2 763.1 102.2 805.5 13.7 805.5 13.7 17.5 102.2 17.5 102.2 60.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="464.9 576.5 464.9 19.5 352.5 19.5 352.5 65.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="345.7 17.5 467.2 17.5 467.2 467.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="307.1" y1="804.4" x2="126.5" y2="804.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="467.3 467.3 467.3 805.4 349 805.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="129.3 73.3 154.3 73.3 154.3 110.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //         <polyline points="144 68.2 144 40.9 154.8 40.9 154.8 77.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //         <line x1="350.5" y1="704.5" x2="350.5" y2="717.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //         <polyline points="350.5 716.2 376.2 716.2 376.2 751.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="368.6 374.2 371 374.2 371 411.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g id="Treppe">
        //         <g>
        //             <line x1="183.5" y1="127.4" x2="183.5" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="187.3" y1="127.4" x2="187.3" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="191.1" y1="127.4" x2="191.1" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="194.9" y1="127.4" x2="194.9" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="198.7" y1="127.4" x2="198.7" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="202.5" y1="127.4" x2="202.5" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="206.2" y1="127.4" x2="206.2" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="210" y1="127.4" x2="210" y2="112.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <g>
        //             <line x1="218.3" y1="128.9" x2="179" y2="128.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="218.3" y1="127.4" x2="179" y2="127.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             </g>
        //         </g>
        //         </g>
        //         <g id="TH">
        //         <g>
        //             <polyline points="128.1 597.3 128.1 554.5 223.4 554.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="185" y1="578" x2="130.2" y2="578" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <polyline points="130.2 599.3 130.2 556.5 223.4 556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="185" y1="575.9" x2="128.1" y2="575.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <polyline points="127.9 607.5 128.1 598.3 185 598.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <polyline points="130 608.5 130.2 599.3 185 599.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="128.1" y1="554.5" x2="128.1" y2="415.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="130.2" y1="554.5" x2="130.2" y2="415.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //         </g>
        //         </g>
        //         <g id="Treppe-2" data-name="Treppe">
        //         <g>
        //             <line x1="158.7" y1="575.9" x2="158.7" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="154.9" y1="575.9" x2="154.9" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="162.5" y1="575.9" x2="162.5" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="147.4" y1="575.9" x2="147.4" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="151.1" y1="575.9" x2="151.1" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.6" y1="575.9" x2="143.6" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="139.8" y1="575.9" x2="139.8" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="136" y1="575.9" x2="136" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="166.3" y1="575.9" x2="166.3" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="170.1" y1="575.9" x2="170.1" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="173.8" y1="575.9" x2="173.8" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="177.6" y1="575.9" x2="177.6" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="181.4" y1="575.9" x2="181.4" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //         </g>
        //         </g>
        //         <g id="Treppe-3" data-name="Treppe">
        //         <g>
        //             <line x1="158.7" y1="597.3" x2="158.7" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="154.9" y1="597.3" x2="154.9" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="162.5" y1="597.3" x2="162.5" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="147.4" y1="597.3" x2="147.4" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="151.1" y1="597.3" x2="151.1" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.6" y1="597.3" x2="143.6" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="139.8" y1="597.3" x2="139.8" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="136" y1="597.3" x2="136" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="166.3" y1="597.3" x2="166.3" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="170.1" y1="597.3" x2="170.1" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="173.8" y1="597.3" x2="173.8" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="177.6" y1="597.3" x2="177.6" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="181.4" y1="597.3" x2="181.4" y2="577.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //         </g>
        //         </g>
        //         <line x1="295" y1="211.2" x2="295" y2="352.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="252.1 368.7 252.1 316 188.7 316" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="297.1 181.6 297.1 144.4 127.9 144.4 127.9 318 178 318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="297.1" y1="352.9" x2="297.1" y2="211.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g id="Treppe-4" data-name="Treppe">
        //         <g>
        //             <line x1="143.7" y1="48.4" x2="154.5" y2="48.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.7" y1="44.6" x2="154.5" y2="44.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.7" y1="52.2" x2="154.5" y2="52.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.7" y1="55.9" x2="154.5" y2="55.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.7" y1="59.7" x2="154.5" y2="59.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //             <line x1="143.7" y1="63.4" x2="154.5" y2="63.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.9px' }} />
        //         </g>
        //         </g>
        //         <line x1="100.2" y1="19.5" x2="127.9" y2="19.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="100.2" y1="17.5" x2="127.9" y2="17.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="296.3" y1="19.5" x2="352.5" y2="19.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="295.1" y1="17.5" x2="352.5" y2="17.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <path id="Toilette_H" data-name="Toilette H" d="M182.1,655.7v-53.5s-49.2,.2-49.2,.2v89s159.4,0,159.4,0v-35.4l-110.1-.2Zm59,8.5c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.4-.4-.5-.7-.1-.3-.2-.6-.2-.9v-2.7c0-.3,0-.6,.2-.9,.1-.2,.2-.5,.4-.7h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.7-.5,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.3,.7,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.7,.1,.3,.2,.6,.2,.9v2.7c0,.3,0,.6-.2,.9-.1,.3-.3,.5-.5,.7-.2,.2-.4,.4-.7,.5-.3,.1-.6,.2-.9,.2h0Zm-1.3,25.7c-.5,0-1-.4-1-1v-21.6c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v8.8c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.8c0-1.4,1.1-2.5,2.5-2.4,1.3,0,2.2,1.2,2.2,2.5v9.7h0v11.8c0,.5-.4,1-1,1h0Zm2.6,0c-.5,0-1-.4-1-1v-21.6c0-1.4,1.1-2.5,2.5-2.4,1.3,0,2.2,1.2,2.2,2.5v8.7c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.8c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v21.6c0,.5-.4,1-1,1h0Z" style={{ fill: '#fff' }} />
        //         <path id="Toilette_D" data-name="Toilette D" d="M190.2,602.4v46h101.9s0-46,0-46h-101.9Zm50.8,14.3c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.4-.4-.5-.7-.1-.3-.2-.6-.2-.9v-2.7c0-.3,0-.6,.2-.9,.1-.2,.2-.5,.4-.7h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.7-.5,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.3,.7,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.7,.1,.3,.2,.6,.2,.9v2.7c0,.3,0,.6-.2,.9-.1,.3-.3,.5-.5,.7-.2,.2-.4,.4-.7,.5-.3,.1-.6,.2-.9,.2h0Zm-1.4,25.8c-.5,0-1-.4-1-1v-7.1h0v-1c0-.2-.2-.4-.4-.4h0s-2,0-2,0c-.3,0-.5,0-.7-.1h0s0,0,0,0c0,0,0,0,0,0-1.1-.5-1.7-1.8-1.3-3l2.6-6.9c.6-1.5,1.2-3.2,1.5-4.3,.2-.6,.8-.8,1.3-.6h0c.5,.2,.8,.8,.6,1.4l-4,10.4-.3,.7c0,.2,0,.4,0,.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0h2c1.3,0,2.3,1.1,2.3,2.4v8.1c0,.6-.4,1-1,1h0Zm1.9-1v-8.1c0-1.3,1.1-2.4,2.3-2.4h0s2,0,2,0c0,0,0,0,.1,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0,.1-.3,0-.5l-.3-.7-4-10.4c-.2-.6,0-1.2,.6-1.4h0c.5-.2,1.1,0,1.3,.6,.4,1.1,1,2.8,1.5,4.3l2.6,6.9c.4,1.2-.1,2.5-1.3,3,0,0,0,0,0,0,0,0,0,0,0,0h0c-.2,0-.4,.1-.7,.1h-2c-.2,0-.4,.2-.4,.4v1h0v7.1c0,.6-.4,1-1,1s-1-.4-1-1Z" style={{ fill: '#fff' }} />
        //         <line x1="298.3" y1="316" x2="287.8" y2="316" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="350.4" y1="805.5" x2="350.4" y2="787.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }} />
        //         <polyline points="90.7 467.8 70.8 467.8 70.8 433.1 79.4 433.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g id="Aufzug">
        //         <polygon points="362.3 721.2 370.9 721.2 370.9 746.6 362.3 746.6 362.3 748.6 373.4 748.6 373.4 719 362.3 719 362.3 721.2" style={{ fill: '#fff' }} />
        //         <rect x="353.7" y="723.2" width="6.6" height="21.4" style={{ fill: '#fff' }} />
        //         <polygon points="360.3 746.6 351.7 746.6 351.7 721.2 360.3 721.2 360.3 719 349.2 719 349.2 748.6 360.3 748.6 360.3 746.6" style={{ fill: '#fff' }} />
        //         <rect x="362.3" y="723.2" width="6.6" height="21.4" style={{ fill: '#fff' }} />
        //         </g>
        //         <g id="Aufzug-2" data-name="Aufzug">
        //         <polygon points="139.9 78.7 148.4 78.7 148.4 104 139.9 104 139.9 106.1 151 106.1 151 76.5 139.9 76.5 139.9 78.7" style={{ fill: '#fff' }} />
        //         <rect x="131.2" y="80.7" width="6.6" height="21.4" style={{ fill: '#fff' }} />
        //         <polygon points="137.9 104 129.2 104 129.2 78.7 137.9 78.7 137.9 76.5 126.7 76.5 126.7 106.1 137.9 106.1 137.9 104" style={{ fill: '#fff' }} />
        //         <rect x="139.9" y="80.7" width="6.6" height="21.4" style={{ fill: '#fff' }} />
        //         </g>
        //         <line x1="364.8" y1="76.7" x2="350.5" y2="76.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="464.9 76.9 464.9 19.5 352.5 19.5 352.5 65.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //     </g>
        //     <g id="Räume_1_OG" data-name="Räume 1 OG">
        //         <g id="1.001" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="467.1" y1="76.7" x2="377.7" y2="76.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="354.6" y1="17.7" x2="354.6" y2="65.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="351.3" y1="76.7" x2="364.7" y2="76.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466.7" y1="18.4" x2="354.5" y2="18.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="465.9" y1="17.9" x2="465.9" y2="75.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <text transform="translate(394.7 53.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan></text>
        //         </g>
        //         <g id="1.002" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="364.8 78.8 352.5 78.8 352.5 101.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466.9" y1="78.8" x2="377.7" y2="78.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466" y1="75.3" x2="466" y2="135.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <text transform="translate(395.4 108.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
        //         </g>
        //         <g id="1.003" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="466.9" y1="134.2" x2="350.2" y2="134.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="467.5" y1="187.6" x2="351.6" y2="187.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.5px' }} />
        //             <line x1="352.5" y1="149.3" x2="352.5" y2="187.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.2px' }} />
        //             <line x1="466.1" y1="134.9" x2="466.1" y2="186.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <text transform="translate(395.4 164.9)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="1.004" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="466.8" y1="243" x2="350.2" y2="243" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="352.5" y1="187.7" x2="352.5" y2="213.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="467.4" y1="189.6" x2="352.5" y2="189.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466" y1="189.9" x2="466" y2="246.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(395 222.5)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>04</tspan></text>
        //         </g>
        //         <g id="1.005" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="352.5" y1="260.1" x2="352.5" y2="298.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466.9" y1="245" x2="350.2" y2="245" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466" y1="245" x2="466" y2="301.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466.6" y1="298.2" x2="351.2" y2="298.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(394.4 277.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //         </g>
        //         <g id="1.006" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="466.8" y1="353.8" x2="350" y2="353.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <polyline points="466.9 300.4 352.5 300.4 352.5 324.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466" y1="301.1" x2="466" y2="357.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(395.4 331.9)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
        //         </g>
        //         <g id="1.007" onClick={(e) => handleRoom(e.currentTarget.id)}> 
        //             <line x1="349.5" y1="411.8" x2="464.8" y2="411.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <polyline points="356.2 372.2 350.5 372.2 350.5 434.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="466.9" y1="355.8" x2="350" y2="355.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="368.6 372.2 373 372.2 373 411.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
        //             <line x1="466" y1="357.1" x2="466" y2="413.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <text transform="translate(395.4 388.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.00</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan></text>
        //         </g>
        //         <g id="1.010" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="464.9 575.7 352.5 575.7 352.5 617.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="352.5" y1="449.9" x2="352.5" y2="525.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="465.5" y1="410.4" x2="465.5" y2="578.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="352.5" y1="413.8" x2="464.9" y2="413.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="352.5" y1="555.4" x2="352.5" y2="575.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="356.2 374.2 352.5 374.2 352.5 434.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(395.5 497.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="24.1" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan></text>
        //             <line x1="352.5" y1="413.8" x2="352.5" y2="434.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="1.011" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="350.9" y1="577.8" x2="466.1" y2="577.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="464.7" y1="633.8" x2="350.2" y2="633.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="352.5" y1="578.8" x2="352.5" y2="616.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="465.3" y1="578.4" x2="465.4" y2="633.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <text transform="translate(392.3 614.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>11</tspan></text>
        //         </g>
        //         <g id="1.012" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="464.5" y1="635.9" x2="350.2" y2="635.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="464.9 689.2 352.5 689.2 352.5 652.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="465.4" y1="635.6" x2="465.4" y2="690.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(395.1 667.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>12</tspan></text>
        //         </g>
        //         <g id="1.013" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="350.6 750.1 350.6 751.5 466.6 751.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="351.5" y1="704.5" x2="351.5" y2="716.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
        //             <polyline points="351.4 715.2 377.2 715.2 377.2 752.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="351.1" y1="690.8" x2="464.6" y2="690.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="465.4" y1="690.2" x2="465.4" y2="752.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(394.9 729.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="23.4" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="1.014" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="350.5 750.1 350.5 753.6 464.9 753.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="465.6 576.8 465.6 803.5 350.6 803.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="352.4" y1="803.4" x2="352.4" y2="787.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }} />
        //             <text transform="translate(394.7 784.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="23.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
        //             <line x1="352.4" y1="803.4" x2="466.1" y2="803.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="465.4" y1="752.8" x2="465.4" y2="805.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="1.015" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="130" y1="694.5" x2="130" y2="803.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.1px' }} />
        //             <line x1="294.4" y1="696.1" x2="130" y2="696.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="294.9" y1="752" x2="294.9" y2="692.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="307.1" y1="803.4" x2="128.8" y2="803.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(200.5 754)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="23.7" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //         </g>
        //         <g id="1.043" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="15.8" y1="746.7" x2="101.6" y2="746.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.9" y1="746.2" x2="15.9" y2="803.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="100.2" y1="763.1" x2="100.2" y2="804.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.5" y1="803.5" x2="101.4" y2="803.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(38.3 779.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="27.7" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="1.044" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="15.8" y1="691.3" x2="102.6" y2="691.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
        //             <line x1="15.8" y1="687.6" x2="15.8" y2="747.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
        //             <line x1="100.2" y1="707.7" x2="100.2" y2="746.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
        //             <line x1="15.8" y1="744.5" x2="100.7" y2="744.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }} />
        //             <text transform="translate(38.1 723.5)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="28" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
        //         </g>
        //         <g id="1.045" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="102.5" y1="689.3" x2="14.5" y2="689.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="14.6 643.3 100.2 643.3 100.2 672.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }} />
        //             <line x1="15.9" y1="644.2" x2="15.9" y2="688" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(38.3 671.5)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="27.7" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //         </g>
        //             <g id="1.046" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="14.7" y1="641.5" x2="101.6" y2="641.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="16" y1="580.6" x2="16" y2="642" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 618)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
        //         </g>
        //         <g id="1.047" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="100.2" y1="541.5" x2="100.2" y2="580" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.6" y1="580" x2="66.3" y2="580" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="66.3" y1="525.1" x2="15.8" y2="525.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="16" y1="524.2" x2="16" y2="580.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="82" y1="525" x2="101.6" y2="524.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="82" y1="580" x2="101.6" y2="580" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 559.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan></text>
        //         </g>
        //         <g id="1.048" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="66.3" y1="523.1" x2="14.5" y2="523.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="100.2 486.2 100.2 523.1 82 523.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.4" y1="469.7" x2="102.1" y2="469.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="468.4" x2="15.8" y2="524.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.048" data-name="1.048" transform="translate(37.9 502.1)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>8</tspan></text>
        //         </g>
        //         <g id="1.049" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="87.9 469.7 68 469.7 68 431.1 79.4 431.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.3" y1="467.8" x2="68" y2="467.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="411.5" x2="15.8" y2="467.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.4" y1="412.8" x2="102.4" y2="412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.049" data-name="1.049" transform="translate(23.6 448.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.7" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="28.4" y="0" style={{ letterSpacing: '0.1em' }}>9</tspan></text>
        //         </g>
        //         <g id="1.050" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.2 372.2 100.2 410.7 13.7 410.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="354.7" x2="15.8" y2="412.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.2" y1="355.8" x2="15.8" y2="355.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.050" data-name="1.050" transform="translate(38.1 387.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="29" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan></text>
        //         </g>
        //         <g id="1.051" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.2 316.8 100.2 353.8 13.7 353.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="299.3" x2="15.8" y2="356.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.2" y1="300.4" x2="15.8" y2="300.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.051" data-name="1.051" transform="translate(38.3 331.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.4" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="28.7" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan></text>
        //         </g>
        //         <g id="1.052" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.2 261.4 100.2 298.5 13.7 298.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.9" y1="243.9" x2="15.9" y2="301.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.3" y1="245" x2="15.9" y2="245" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.052" data-name="1.052" transform="translate(38.1 275.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="29" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
        //         </g>
        //         <g id="1.053" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.2 206 100.2 243.1 13.7 243.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="189.3" x2="15.8" y2="243.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.4" y1="189.5" x2="15.9" y2="189.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.053" data-name="1.053" transform="translate(38.1 221.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="29" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="1.054" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="13.7 187.6 100.2 187.6 100.2 150.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.6" y1="134" x2="15.6" y2="187.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.2" y1="134.1" x2="15.8" y2="134.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.054" data-name="1.054" transform="translate(38.1 165.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="29" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
        //         </g>
        //         <g id="1.055" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="13.7 132.2 100.2 132.2 100.2 95.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="78.6" x2="15.8" y2="132.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.3" y1="78.8" x2="15.9" y2="78.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.055" data-name="1.055" transform="translate(38.3 111.1)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.</tspan><tspan x="9.7" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="19.4" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="28.7" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //         </g>
        //         <g id="1.056" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="102.6" y1="76.8" x2="13.7" y2="76.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="100.2" y1="19.5" x2="100.3" y2="60.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.8" y1="19.4" x2="15.8" y2="73.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="102.3" y1="19.6" x2="15.9" y2="19.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.056" data-name="1.056" transform="translate(38.1 51.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>56</tspan></text>
        //         </g>
        //         <g id="1.028" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="295.1 101.7 295.1 109.2 128.3 109.2 128.3 107.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="127.9 19.5 295.1 19.5 295.1 64.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="130" y1="74.8" x2="130" y2="19.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="127.9 71.4 155.6 71.4 155.6 108.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="143 68.1 143 39.4 156.2 39.4 156.2 77.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.6px' }} />
        //             <line x1="129" y1="19.6" x2="296.2" y2="19.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(200.4 69.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan><tspan x="26.9" y="0" style={{ letterSpacing: '0.1em' }}>8</tspan></text>
        //         </g>
        //         <g id="1.025" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="178 316 130 316 130 146.4 295 146.4 295 181.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="295" y1="211.2" x2="295" y2="316.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(200.2 237.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.02</tspan><tspan x="27.2" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //         </g>
        //         <g id="1.034" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="130" y1="413.7" x2="181.1" y2="413.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }} />
        //             <line x1="130" y1="318" x2="130" y2="415.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="202.6" y1="368.5" x2="202.6" y2="318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="188.7" y1="318" x2="204.1" y2="318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="128.8" y1="318.1" x2="178" y2="318.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.034" data-name="1.034" transform="translate(149.5 363.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.0</tspan><tspan x="19" y="0" style={{ letterSpacing: '0.1em' }}>34</tspan></text>
        //         </g>
        //         <g id="1.023" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="204.6" y1="368.5" x2="204.6" y2="316" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="250.3" y1="382.5" x2="250.3" y2="413.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="202.9 318 250.4 318 250.4 368.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.5px' }} />
        //             <text id="_1.023" data-name="1.023" transform="translate(209.2 362.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.02</tspan><tspan x="27.2" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="1.022" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="295 382.5 295 413.7 252.7 413.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }} />
        //             <line x1="298.3" y1="318" x2="287.8" y2="318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="252.1" y1="316" x2="252.1" y2="368.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="252" y1="383.3" x2="252" y2="414" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="295" y1="318.1" x2="295" y2="352.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text id="_1.022" data-name="1.022" transform="translate(255.9 361.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold',  fontWeight: ' 700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan><tspan x="5" y="0" style={{ letterSpacing: '0.1em' }}>.02</tspan><tspan x="27.2" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
        //         </g>
        //     </g>
        // </svg>
    )
    
}