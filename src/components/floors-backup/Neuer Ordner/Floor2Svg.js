import { useEffect, useRef } from "react";

import Floor2  from './floors-svg/Grundriss_2OG.svg'

export function Floor2Svg({room, handleRoom}) {
    
    const svg = useRef(null);
    
    // useEffect(() => {

    //     if (svg.current == null) 
    //     return

    //     svg.current.contentDocument.documentElement.querySelectorAll('#rooms g').forEach(el => {
    //         // console.log(el)

    //         el.querySelector('text').style.fill = 'white'
    //         el.querySelector('rect').style.fill = 'none'

    //         el.addEventListener('click', (e) => {
    //             console.log(el.id)
    //             e.preventDefault()
    //             el.querySelector('rect').style.fill = 'white'
    //             el.querySelector('text').style.fill = '#1C6596'
    //             handleRoom(el.id)
    //         })
    //     })
    // })

    useEffect(() => {
        console.log('room', room)

        if (svg.current == null) 
        return

        svg.current.contentDocument.documentElement.querySelectorAll('#rooms g').forEach(el => {
            el.querySelector('text').style.fill = 'white'
            el.querySelector('rect').style.fill = 'none'

            el.addEventListener('click', (e) => {
                console.log(el.id)
                e.preventDefault()
                el.querySelector('rect').style.fill = 'white'
                el.querySelector('text').style.fill = '#1C6596'
                handleRoom(el.id)
            })
        })

        if (room == null)
        return


        
        let group = svg.current.contentDocument.documentElement.getElementById(`${room}`)
        if (group == null) 
        return
        
        group.querySelector('rect').style.fill = 'white'
        group.querySelector('text').style.fill = '#1C6596'
    }, [room, svg])

    return (
        <object ref={svg} data={Floor2}></object>

        // <svg 
        //     xmlns="http://www.w3.org/2000/svg" 
        //     viewBox="0 0 481.9 822"
        //     width="100%" //481.9px
        //     height="100%" //822px
        //     ref={svg}
        // >
        //     <g id="Räume_2_OG" data-name="Räume 2 OG">
        //         <g id="2.056" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="102.9" y1="76.8" x2="14.2" y2="76.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 54.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="22.8" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="32.2" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
        //             <line x1="14.2" y1="76.8" x2="14.2" y2="18.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="101.2" y1="18.2" x2="14.2" y2="18.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="101.2" y1="60.3" x2="101.2" y2="18.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.055" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             {/* <polyline points="14.2 132.2 100.7 132.2 100.7 95.2" style={{ fill: 'red', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} /> */}
        //             <line x1="102.9" y1="78.9" x2="16.3" y2="78.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 109.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="22.8" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="32.2" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //             <line x1="16.3" y1="78.9" x2="16.3" y2="132.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.054" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             {/* <polyline points="16.3 187.6 100.7 187.6 100.7 150.6" style={{ fill: 'red', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} /> */}
        //             <text transform="translate(37.9 165.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="32.6" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan></text>
        //             <line x1="104.2" y1="132.7" x2="16.3" y2="132.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="16.3" y1="132.7" x2="16.3" y2="187.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.053" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.7 206 100.7 243.1 14.2 243.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 221)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="32.6" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //             <line x1="15.3" y1="189.7" x2="15.3" y2="243.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.3" y1="189.7" x2="102.8" y2="189.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.052" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.7 261.4 100.7 298.5 14.2 298.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 276.6)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="32.6" y="0" style={{ letterSpacing: '0.1em' }}>2</tspan></text>
        //             <polyline points="15.3 298.5 15.3 244.4 101.6 244.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.051" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="15.3 352.5 15.3 298.5 101.6 298.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="100.7 316.8 100.7 353.8 14.2 353.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 332.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="22.8" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan><tspan x="32.2" y="0" style={{ letterSpacing: '0.1em' }}>1</tspan></text>
        //         </g>
        //         <g id="2.050" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="15.3 408.7 15.3 354.7 101.6 354.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="100.7 372.2 100.7 410.7 14.2 410.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 388.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>05</tspan><tspan x="32.6" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan></text>
        //         </g>
        //         <g id="2.049" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="75.9 469.9 75.9 431.1 82.2 431.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="113.3" y1="412.7" x2="16" y2="412.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(24 446.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>9</tspan></text>
        //             <line x1="16" y1="412.7" x2="16" y2="468.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="75.9" y1="468.6" x2="16" y2="468.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.048" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="63.3" y1="523.1" x2="14.2" y2="523.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="100.7 486.2 100.7 523.1 78.3 523.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 499.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>8</tspan></text>
        //             <line x1="15" y1="468.6" x2="102.8" y2="468.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15" y1="523.1" x2="15" y2="468.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.047" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="100.7" y1="541.5" x2="100.7" y2="578.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.2" y1="578.5" x2="63.3" y2="578.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="63.3" y1="525.1" x2="15.3" y2="525.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 557.1)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>7</tspan></text>
        //             <line x1="78.3" y1="524" x2="102.8" y2="524" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.3" y1="525.1" x2="15.3" y2="578.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="78.3" y1="578.5" x2="100.7" y2="578.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.046" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <line x1="15.3" y1="580.6" x2="15.3" y2="641.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="14.2" y1="641.4" x2="102.9" y2="641.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="78.3 580.6 100.7 580.6 100.7 625.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.3" y1="580.6" x2="63.3" y2="580.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 617.4)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>6</tspan></text>
        //         </g>
        //         <g id="2.045" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.7 659.9 100.7 689.3 15.3 689.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <line x1="15.3" y1="643.5" x2="102.9" y2="643.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 670.2)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>5</tspan></text>
        //             <line x1="15.3" y1="643.5" x2="15.3" y2="689.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="2.044" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.7 690.7 15.3 690.7 15.3 744.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <polyline points="100.7 707.6 100.7 744.7 100.7 744.7 14.2 744.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 723.7)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>044</tspan></text>
        //         </g>
        //         <g id="2.043" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="100.7 745.8 15.3 745.8 15.3 803.5 100.7 803.5 100.7 763.1 100.7 803.5 16.3 803.5 16.3 19.5 100.7 19.5 100.7 60.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(37.9 781.8)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0">4</tspan><tspan x="31.9" y="0" style={{ letterSpacing: '0.1em' }}>3</tspan></text>
        //         </g>
        //         <g id="2.040" onClick={(e) => handleRoom(e.currentTarget.id)}>
        //             <polyline points="128.4 694.1 179.1 694.1 179.1 611.5 130.6 611.5 130.6 669.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //             <text transform="translate(134.7 660.3)" style={{ fill: '#fff', fontFamily: 'Typ1451-Bold', fontWeight: '700' }}><tspan x="0" y="0" style={{ letterSpacing: '0.1em' }}>2.</tspan><tspan x="13" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan><tspan x="23.1" y="0" style={{ letterSpacing: '0.1em' }}>4</tspan><tspan x="31.6" y="0" style={{ letterSpacing: '0.1em' }}>0</tspan></text>
        //         </g>
        //     </g>

        //     <g id="Grundriss_2_OG" data-name="Grundriss 2 OG">
        //         <polyline points="130.4 696.1 181.2 696.1 181.2 609.5 128.5 609.5 128.5 669.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="102.8" y1="541.5" x2="102.8" y2="578.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="185.1" y1="109.2" x2="130.3" y2="109.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <polyline points="132.6 413 128.5 413 128.5 318" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 707.6 102.8 746.7 14.9 746.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 659.9 102.8 691.3 16.3 691.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="78.3 578.5 102.8 578.5 102.8 625.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 486.2 102.8 525.1 78.3 525.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="16.3 469.9 102.8 469.9 102.8 431.1 96.1 431.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="14.2 467.8 100.7 467.8 100.7 433.1 96.1 433.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 372.2 102.8 412 16.3 412" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 316.8 102.8 355.8 16.3 355.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 261.4 102.8 300.5 16.3 300.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 206 102.8 245.1 16.3 245.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="16.3 189.7 102.8 189.7 102.8 150.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="16.3 134.3 102.8 134.3 102.8 95.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="128.5 683.1 128.5 805.5 135.7 805.5 467.8 805.5 467.8 17.5 128.5 17.5 128.5 52.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="102.8 763.1 102.8 805.5 14.2 805.5 14.2 17.5 102.8 17.5 102.8 60.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="130.6 52.6 130.6 19.5 465.7 19.5 465.7 803.5 135.7 803.5 130.6 803.5 130.6 683.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="130.3 74.9 157.8 74.9 157.8 109.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="130.3 72.9 159.9 72.9 159.9 108.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <polyline points="147 66.3 147 37.2 157.8 37.2 157.8 78.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <polyline points="145 66.3 145 35.1 159.9 35.1 159.9 81.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <polyline points="77.9 467.8 77.9 433.1 82.2 433.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g>
        //         <line x1="188" y1="144.3" x2="188" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="191.7" y1="144.3" x2="191.7" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="195.5" y1="144.3" x2="195.5" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="199.3" y1="144.3" x2="199.3" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="203.1" y1="144.3" x2="203.1" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="206.9" y1="144.3" x2="206.9" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="210.7" y1="144.3" x2="210.7" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="214.4" y1="144.3" x2="214.4" y2="127.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         </g>
        //         <g>
        //         <polyline points="130.6 611.5 130.6 575.9 186.6 575.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="132.6 611.5 132.6 578 186.6 578" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="130.6 318 130.6 556.5 186.6 556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="130.6 413 132.6 413 132.6 554.5 186.6 554.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         </g>
        //         <g id="Treppe">
        //         <g>
        //             <line x1="159.3" y1="575.9" x2="159.3" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="155.5" y1="575.9" x2="155.5" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="163.1" y1="575.9" x2="163.1" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="148" y1="575.9" x2="148" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="151.8" y1="575.9" x2="151.8" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="166.9" y1="575.9" x2="166.9" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="170.7" y1="575.9" x2="170.7" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="174.5" y1="575.9" x2="174.5" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="178.2" y1="575.9" x2="178.2" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="182" y1="575.9" x2="182" y2="556.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         </g>
        //         </g>
        //         <polyline points="130.6 323.5 130.6 146.4 238.4 146.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <polyline points="238.4 144.4 128.5 144.4 128.5 319.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g id="Treppe-2" data-name="Treppe">
        //         <g>
        //             <line x1="147" y1="44.7" x2="157.8" y2="44.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="147" y1="40.9" x2="157.8" y2="40.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="147" y1="48.5" x2="157.8" y2="48.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="147" y1="52.2" x2="157.8" y2="52.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="147" y1="56" x2="157.8" y2="56" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //             <line x1="147" y1="59.7" x2="157.8" y2="59.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         </g>
        //         </g>
        //         <polyline points="219 127.2 182.1 127.2 182.1 126.9 182.1 108.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <polyline points="130.3 111.3 184.2 111.3 184.2 124.8 184.2 125.2 219 125.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }} />
        //         <line x1="130.6" y1="19.5" x2="100.7" y2="19.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="128.5" y1="17.5" x2="102.8" y2="17.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="113.3" y1="410.7" x2="16" y2="410.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="139.9" y1="611.4" x2="123.6" y2="611.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="139.9" y1="609.5" x2="123.6" y2="609.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="137.9" y1="805.5" x2="121.6" y2="805.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="137.9" y1="803.5" x2="121.6" y2="803.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="144.2" y1="110.1" x2="144.2" y2="120.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="142.2" y1="110.1" x2="142.2" y2="120.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="144.2" y1="134.3" x2="144.2" y2="147.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <line x1="142.2" y1="134.3" x2="142.2" y2="147.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }} />
        //         <g id="Treppe-3" data-name="Treppe">
        //         <polygon points="143.9 79.9 152.5 79.9 152.5 104.4 143.9 104.4 143.9 106.4 155 106.4 155 77.8 143.9 77.8 143.9 79.9" style={{ fill: '#fff' }} />
        //         <rect x="135.1" y="81.8" width="6.7" height="20.6" style={{ fill: '#fff' }} />
        //         <polygon points="141.8 104.4 133.1 104.4 133.1 79.9 141.8 79.9 141.8 77.8 130.6 77.8 130.6 106.4 141.8 106.4 141.8 104.4" style={{ fill: '#fff' }} />
        //         <rect x="143.9" y="81.8" width="6.6" height="20.6" style={{ fill: '#fff' }} />
        //         </g>
        //     </g>
        // </svg>
    )
}