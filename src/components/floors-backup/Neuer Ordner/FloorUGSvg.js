export function FloorEGSvg({handleRoom}) {

    return (
        <svg 
            viewBox="0 0 453.9 786"
            width="100%" //481.9px
            height="100%" //822px
            xmlns="http://www.w3.org/2000/svg"            
        >
            <g id="Grundriss_UG" data-name="Grundriss UG" transform="matrix(1, 0, 0, 1, -55.200001, -120)">
                <path id="Toilette_Damen" data-name="Toilette Damen" d="M266.8,783.7h65.6v-59h-65.6v59Zm31.5-15.3c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-6.8h0v-.9c0-.2-.2-.4-.4-.4h0s-2.1,0-2.1,0c-.3,0-.5,0-.7-.1h0s0,0,0,0c0,0,0,0,0,0-1.1-.5-1.7-1.7-1.3-2.9l2.6-6.6c.6-1.5,1.2-3.1,1.6-4.1,.2-.5,.8-.8,1.3-.6h0c.5,.2,.8,.8,.6,1.3l-4,10-.3,.7c0,.1,0,.3,0,.4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0h2c1.3,0,2.4,1,2.4,2.3v7.8Zm.5-23.7c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.4-.4-.5-.7-.1-.3-.2-.5-.2-.8v-2.6c0-.3,0-.6,.2-.8,.1-.2,.2-.4,.4-.6h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.7-.4,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.2,.7,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.6,.1,.3,.2,.5,.2,.8v2.6c0,.3,0,.6-.2,.8-.1,.3-.3,.5-.5,.7-.2,.2-.4,.3-.7,.5-.3,.1-.6,.2-.9,.2h0Zm5.7,15.4s0,0,0,0c0,0,0,0,0,0h0c-.2,0-.4,.1-.7,.1h-2.1c-.2,0-.4,.2-.4,.4v.9h0v6.8c0,.5-.4,1-1,1s-1-.4-1-1v-7.8c0-1.3,1.1-2.3,2.4-2.3h0s2,0,2,0c0,0,0,0,.1,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0,.1-.3,0-.4l-.3-.7-4-10c-.2-.5,0-1.1,.6-1.3h0c.5-.2,1.1,0,1.3,.6,.4,1,1,2.6,1.6,4.1l2.6,6.6c.4,1.1-.1,2.4-1.3,2.9Z" style={{fill: '#fff'}}/>
                <path id="Toilette_Damen-2" data-name="Toilette Damen" d="M60.2,213h76.8v-36.1H60.2v36.1Zm37.8-2.7c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-7.1s0,0,0,0v-1c0-.2-.2-.4-.4-.4h0s-2,0-2,0c-.3,0-.5,0-.7-.1h0s0,0,0,0c0,0,0,0,0,0-1.1-.5-1.7-1.8-1.3-3l2.5-6.9c.5-1.5,1.2-3.2,1.5-4.2,.2-.5,.8-.8,1.3-.6h0c.5,.2,.8,.8,.6,1.4l-3.9,10.3-.3,.7c0,.2,0,.4,0,.4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0h2c1.3,0,2.3,1.1,2.3,2.4v8.1Zm.4-24.5c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.4-.4-.5-.7-.1-.3-.2-.6-.2-.9v-2.7c0-.3,0-.6,.2-.9,.1-.2,.2-.5,.4-.6h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.6-.5,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.3,.6,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.7,.1,.3,.2,.6,.2,.9v2.7c0,.3,0,.6-.2,.9-.1,.3-.3,.5-.5,.7-.2,.2-.4,.4-.7,.5-.3,.1-.6,.2-.9,.2h0Zm5.6,16s0,0,0,0c0,0,0,0,0,0h0c-.2,0-.4,.1-.7,.1h-2c-.2,0-.4,.2-.4,.4v1h0s0,7.1,0,7.1c0,.5-.4,1-1,1s-1-.4-1-1v-8c0-1.3,1-2.4,2.3-2.4h0s2,0,2,0c0,0,0,0,.1,0,0,0,0,0,0,0,0,0,0,0,0,0,.1,0,.1-.3,0-.4l-.3-.7-3.9-10.3c-.2-.5,0-1.2,.6-1.4h0c.5-.2,1.1,0,1.3,.6,.4,1,1,2.7,1.5,4.2l2.5,6.9c.4,1.2-.1,2.5-1.3,3Z" style={{fill: '#fff'}}/>
                <polyline points="169.8 479.8 169.8 508.9 298.7 508.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="198.5" y1="722.1" x2="167.7" y2="722.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="198.5" y1="720.1" x2="169.8" y2="720.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="242.1" y1="722.1" x2="198.5" y2="722.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="242.1" y1="720.1" x2="198.5" y2="720.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="327" y1="720.1" x2="242.1" y2="720.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="327" y1="722.1" x2="242.1" y2="722.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="335.2" y1="722.1" x2="325.7" y2="722.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="337.2" y1="720.1" x2="325.7" y2="720.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="390.9 714.5 391.2 795.1 420.6 795.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }}/>
                <line x1="393" y1="341.1" x2="393" y2="410.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="393" y1="302.7" x2="393" y2="341.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="393" y1="274" x2="393" y2="305" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="346.6" y1="834.1" x2="333" y2="834.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }}/>
                <line x1="346.6" y1="836.1" x2="333.8" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }}/>
                <polyline points="393.3 894 393.3 904.2 381.7 904.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }}/>
                <polyline points="395.3 894 395.3 906 381.7 906" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }}/>
                <polyline points="346.5 903.8 335 903.8 335 890.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="346.5 905.8 333 905.8 333 890.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="346.6" y1="563.7" x2="337.2" y2="563.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="346.6" y1="565.8" x2="335.2" y2="565.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="563.7" x2="381.7" y2="563.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="393" y1="565.8" x2="381.7" y2="565.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="337.4" y1="616.2" x2="337.4" y2="703.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="337.4" y1="508.9" x2="337.4" y2="590.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="599.5" x2="167.7" y2="508.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="703.3" x2="167.7" y2="614.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="346.6" y1="208.7" x2="331.4" y2="208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="208.7" x2="381.7" y2="208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '1.8px' }}/>
                <line x1="390.9" y1="652.9" x2="390.9" y2="668.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="550.2" x2="390.9" y2="637.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="321.1" y1="246.2" x2="321.1" y2="235.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="323.2" y1="244.2" x2="323.2" y2="235.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="321.1" y1="217.3" x2="321.1" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="323.2" y1="217.3" x2="323.2" y2="208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="184.5" y1="246.2" x2="184.5" y2="235.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="182.5" y1="244.2" x2="182.5" y2="235.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="184.5" y1="217.3" x2="184.5" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="182.5" y1="217.3" x2="182.5" y2="208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="506" x2="146.3" y2="506" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="508" x2="146.3" y2="508" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="163.3" y1="506" x2="167.7" y2="506" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="163.3" y1="508" x2="167.7" y2="508" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="821" x2="146.3" y2="821" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="823" x2="146.3" y2="823" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="892" x2="169.8" y2="892" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="121.4" x2="169.8" y2="121.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="330.3" y1="121.4" x2="393.4" y2="121.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="163.3" y1="821" x2="169.6" y2="821" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="163.3" y1="823" x2="167.7" y2="823" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="614.2" x2="141.9" y2="628.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="643.3" x2="141.9" y2="670.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="544.9" x2="141.9" y2="592.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="452" x2="141.9" y2="461.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="508" x2="141.9" y2="513" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="80.4" y1="582.5" x2="80.4" y2="599.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="78.4" y1="584.5" x2="78.4" y2="599.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="103" y1="582.5" x2="103" y2="599.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="101" y1="584.5" x2="101" y2="599.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="55.2" y1="584.5" x2="141.9" y2="584.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="118.1" y1="670.9" x2="118.1" y2="684" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="200.7" x2="141.9" y2="232.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="200.7" x2="139.9" y2="232.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="85" y1="166.1" x2="85" y2="174.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="85" y1="121.4" x2="85" y2="153.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="172.2" x2="167.7" y2="157.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="300.3 208.7 167.7 208.7 167.7 205.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="331.4" y1="208.7" x2="316" y2="208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="300.3 206.7 169.8 206.7 169.8 205.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="390 836.1 457.5 836.1 457.5 894" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="298.7" y1="510.9" x2="291.2" y2="510.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="397.6" x2="337.4" y2="397.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="277.1" x2="167.7" y2="464.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="337.4 258.8 337.4 244.2 167.7 244.2 167.7 258.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="337.4" y1="439.2" x2="337.4" y2="277.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="335.3 466.6 335.3 508.9 218.9 508.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="335.3" y1="277.2" x2="335.3" y2="439.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="169.8 258.8 169.8 246.2 335.3 246.2 335.3 258.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="169.8" y1="464.5" x2="169.8" y2="277.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="335.3" y1="395.6" x2="169.8" y2="395.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="420" y1="256.5" x2="420" y2="288.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="399.4" y1="233.3" x2="391.2" y2="233.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }}/>
                <line x1="413.4" y1="233.3" x2="422" y2="233.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }}/>
                <polyline points="462.8 507 462.8 487.7 420.3 487.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="464.9 505.3 464.9 486 420.3 486" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="412.6" y1="413.3" x2="412.6" y2="452" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="393" y1="437.9" x2="393" y2="470.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="437.9" x2="390.9" y2="470.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="406.2 486 390.9 486 390.9 522.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="406.2 488 393 488 393 522.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="697.8" x2="139.9" y2="790.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="55.2 727.4 118.1 727.4 118.1 697.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="697.8" x2="141.9" y2="790.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="55.2 670.9 141.9 670.9 141.9 684" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="616.4" x2="55.2" y2="616.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="614.2" x2="139.9" y2="619.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="141.9 526.6 141.9 544.9 135 544.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="141.9 476.6 141.9 508 55.2 508" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="139.9 476.6 139.9 506 55.2 506" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="141.9 367.1 141.9 452 102 452" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="55.2 259.6 141.9 259.6 141.9 299.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="141.9 168.1 141.9 173.8 55.2 173.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="217.8" x2="55.2" y2="217.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="215.8" x2="55.2" y2="215.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="167.7 143.1 167.7 121.4 332 121.4 332 206.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="302.7" x2="390.9" y2="410.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="274" x2="390.9" y2="305" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="390.9" y1="666.2" x2="390.9" y2="687" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="337.4 770.9 337.4 701.3 325.7 701.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="167.7 812.1 167.7 890 333.4 890 333.4 811.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="335.3 770.9 335.3 703.3 325.7 703.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="199.3 788.8 337.4 788.8 337.4 783.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="262" y1="722.1" x2="262" y2="788.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="199.3 786.8 335.3 786.8 335.3 783.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="318.9" y1="821" x2="333.4" y2="821" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.7px' }}/>
                <line x1="207.3" y1="821" x2="291" y2="821" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="167.7" y1="821" x2="193.3" y2="821" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="327 703.3 169.8 703.3 169.8 786.8 186 786.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="264" y1="720.1" x2="264" y2="786.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="139.9 816 139.9 892 55.2 892 55.2 121.4 139.9 121.4 139.9 155.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="385.8 123.4 507.7 123.4 507.7 667.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="163.3" y1="123.4" x2="386.3" y2="123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="168.7 890 57.3 890 57.3 123.4 164.5 123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <g id="Treppe">
                <line x1="406.7" y1="861.1" x2="406.7" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="402.9" y1="861.1" x2="402.9" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="410.5" y1="861.1" x2="410.5" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="414.3" y1="861.1" x2="414.3" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="418.1" y1="861.1" x2="418.1" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="421.9" y1="861.1" x2="421.9" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="425.7" y1="861.1" x2="425.7" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="429.5" y1="861.1" x2="429.5" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="406.7" y1="891.7" x2="406.7" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="402.9" y1="891.7" x2="402.9" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="399.1" y1="891.7" x2="399.1" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="410.5" y1="891.7" x2="410.5" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="414.3" y1="891.7" x2="414.3" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="418.1" y1="891.7" x2="418.1" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="421.9" y1="891.7" x2="421.9" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="425.7" y1="891.7" x2="425.7" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="429.5" y1="891.7" x2="429.5" y2="863.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="433.3" y1="861.1" x2="433.3" y2="836.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <polyline points="168.7 170.8 194.4 170.8 194.4 206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                <line x1="420.3" y1="836.1" x2="420.3" y2="809.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                <g>
                <polyline points="437.8 863.1 395.3 863.1 395.3 892 505.7 892 505.7 666.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="437.8 861.1 393.3 861.1 393.3 894 507.7 894 507.7 665.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <g id="Treppe-2" data-name="Treppe">
                <polyline points="227.2 244.2 227.2 227.4 227.2 227.4 292.4 227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <polyline points="225.1 246.2 225.1 225.4 292.4 225.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <g>
                    <line x1="249.9" y1="244.2" x2="249.9" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="253.7" y1="244.2" x2="253.7" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="246.1" y1="244.2" x2="246.1" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="242.3" y1="244.2" x2="242.3" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="238.5" y1="244.2" x2="238.5" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="234.8" y1="244.2" x2="234.8" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="231" y1="244.2" x2="231" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <g>
                    <line x1="284.1" y1="244.2" x2="284.1" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="287.8" y1="244.2" x2="287.8" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="280.3" y1="244.2" x2="280.3" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="276.5" y1="244.2" x2="276.5" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="272.7" y1="244.2" x2="272.7" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="268.9" y1="244.2" x2="268.9" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="265.1" y1="244.2" x2="265.1" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="261.3" y1="244.2" x2="261.3" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="257.5" y1="244.2" x2="257.5" y2="227.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    </g>
                </g>
                </g>
                <g>
                <line x1="139.9" y1="710.5" x2="118.1" y2="710.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="706.8" x2="118.1" y2="706.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="714.2" x2="118.1" y2="714.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="717.9" x2="118.1" y2="717.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="721.6" x2="118.1" y2="721.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="139.9" y1="725.2" x2="118.1" y2="725.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <polyline points="507.6 121.5 391.2 121.5 391.2 245.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                <line x1="141.9" y1="121.4" x2="141.9" y2="155.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <line x1="141.9" y1="816" x2="141.9" y2="891.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                <rect x="505.4" y="120.6" width="4.4" height="3.1" transform="translate(385.4 629.8) rotate(-90)" style={{fill: '#fff'}}/>
                <path id="Toilette_Herren" data-name="Toilette Herren" d="M60.2,256.5h76.8v-35.7H60.2v35.7Zm38.8-14.5h0v11.6c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-21.2c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v8.6c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.7c0-1.3,1.1-2.4,2.4-2.3,1.2,0,2.1,1.2,2.1,2.4v9.5Zm.3-12.7c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.3-.4-.5-.7-.1-.3-.2-.6-.2-.9v-2.7c0-.3,0-.6,.2-.9,.1-.2,.2-.5,.4-.6h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.6-.4,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.3,.6,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.7,.1,.3,.2,.6,.2,.9v2.7c0,.3,0,.6-.2,.9-.1,.3-.3,.5-.5,.7-.2,.2-.4,.4-.7,.5-.3,.1-.6,.2-.9,.2h0Zm4.9,11.7c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.6c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v21.2c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-21.2c0-1.3,1.1-2.4,2.4-2.3,1.2,0,2.1,1.2,2.1,2.4v8.6Z" style={{fill: '#fff'}}/>
                <path id="Toilette_Herren-2" data-name="Toilette Herren" d="M172.5,783.7h86.7v-59h-86.7v59Zm43.1-26.1h0v11.4c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-20.8c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v8.5c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.5c0-1.3,1.2-2.4,2.5-2.3,1.3,0,2.2,1.2,2.2,2.4v9.4Zm.3-12.5c-.3,0-.6,0-.9-.2-.3-.1-.5-.3-.7-.5-.2-.2-.4-.4-.5-.7-.1-.3-.2-.5-.2-.8v-2.6c0-.3,0-.6,.2-.8,.1-.2,.3-.4,.4-.6h0s0,0,0,0c0,0,0,0,0,0h0c.2-.2,.4-.3,.7-.4,.3-.1,.6-.2,.9-.2s.6,0,.9,.2c.2,.1,.5,.2,.7,.4h0s0,0,0,0h0s0,0,0,0h0c.2,.2,.3,.4,.4,.6,.1,.3,.2,.5,.2,.8v2.6c0,.3,0,.6-.2,.8-.1,.3-.3,.5-.5,.7-.2,.2-.4,.3-.7,.5-.3,.1-.6,.2-.9,.2h0Zm5.1,11.5c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-8.5c0-.2-.2-.4-.4-.4s-.4,.2-.4,.4v20.8c0,.5-.4,1-1,1h0c-.5,0-1-.4-1-1v-20.8c0-1.3,1.2-2.4,2.5-2.3,1.3,0,2.2,1.1,2.2,2.4v8.4Z" style={{fill: '#fff'}}/>
                <g id="Aufzug">
                <polygon points="180.2 175.8 189 175.8 189 201.8 180.2 201.8 180.2 203.9 191.6 203.9 191.6 173.5 180.2 173.5 180.2 175.8" style={{fill: '#fff'}}/>
                <rect x="171.3" y="177.8" width="6.8" height="21.9" style={{fill: '#fff'}}/>
                <polygon points="178.1 201.8 169.2 201.8 169.2 175.8 178.1 175.8 178.1 173.5 166.6 173.5 166.6 203.9 178.1 203.9 178.1 201.8" style={{fill: '#fff'}}/>
                <rect x="180.2" y="177.8" width="6.8" height="21.9" style={{fill: '#fff'}}/>
                </g>
                <g id="Aufzug-2" data-name="Aufzug">
                <polygon points="404.9 800.4 414.3 800.4 414.3 829 404.9 829 404.9 831.4 417.1 831.4 417.1 798 404.9 798 404.9 800.4" style={{fill: '#fff'}}/>
                <rect x="395.3" y="802.7" width="7.3" height="24.1" style={{fill: '#fff'}}/>
                <polygon points="402.7 829 393.1 829 393.1 800.4 402.7 800.4 402.7 798 390.3 798 390.3 831.4 402.7 831.4 402.7 829" style={{fill: '#fff'}}/>
                <rect x="404.9" y="802.7" width="7.3" height="24.1" style={{fill: '#fff'}}/>
                </g>
                <line x1="85.7" y1="123.4" x2="140.9" y2="123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
            </g>
            <g id="Räume_UG" data-name="Räume UG" transform="matrix(1, 0, 0, 1, -55.200001, -120)">
                <g id="1.101" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text transform="translate(372.8 266.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em'}}><tspan x="0" y="0">-1.101</tspan></text>
                </g>
                <g id="1.001" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="505.7" y1="177.8" x2="450.9" y2="177.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="399.4" y1="231.3" x2="393" y2="231.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="413.4" y1="231.3" x2="422" y2="231.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="436.8 177.8 420 177.8 420 242.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="505.7 511 505.7 121.4 393 121.4 393 245.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="122.5" x2="505.7" y2="177.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text id="_-1.001" data-name="-1.001" transform="translate(440.4 153.9) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.001</tspan></text>
                </g>
                <g id="1.003" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="507.7" y1="179.8" x2="450.9" y2="179.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                    <line x1="422" y1="256.5" x2="422" y2="286.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="436.8 179.8 422 179.8 422 242.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="286.6" x2="390.9" y2="286.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="178.5" x2="505.7" y2="286.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                    <text transform="translate(439.9 237.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.003</tspan></text>
                </g>
                <g id="1.004" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <polyline points="505.7 341.1 412.6 341.1 412.6 380" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.9" y1="288.6" x2="390.9" y2="288.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.9px' }}/>
                    <text transform="translate(440 320.6) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.004</tspan></text>
                    <line x1="505.7" y1="287.6" x2="505.7" y2="342.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <g id="1.005" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <polyline points="505.7 343.1 414.6 343.1 414.6 380" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="395.5" x2="412" y2="395.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="343.1" x2="505.7" y2="396.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(440.1 374.4) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.005</tspan></text>
                </g>
                <g id="1.006" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="505.7" y1="450" x2="390.4" y2="450" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="397.5" x2="412" y2="397.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="414.6" y1="413.3" x2="414.6" y2="450" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="396.5" x2="505.7" y2="449.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(440 430.3) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.006</tspan></text>
                </g>
                <g id="1.102" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text transform="translate(365.8 430) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.102</tspan></text>
                </g>
                <g id="1.007" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="393" y1="505" x2="505.9" y2="505" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.9" y1="452" x2="390.4" y2="452" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.3" y1="450.8" x2="505.3" y2="507" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(439.3 477) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.007</tspan></text>
                </g>
                <g id="1.008" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="507.1" y1="667.2" x2="391.6" y2="667.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="393" y1="507" x2="505.3" y2="507" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="393" y1="652.9" x2="393" y2="668.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="393" y1="550.2" x2="393" y2="637.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="509" x2="505.7" y2="668.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(427.6 599.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.008</tspan></text>
                </g>
                <g id="1.011" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="446.6" y1="779.9" x2="505.7" y2="779.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="505.7 668.2 393 668.2 393 687" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="393 714.5 393.3 793.1 420.6 793.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="420.3 809.2 420.3 779.9 432.5 779.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="505.7" y1="668.2" x2="505.7" y2="779.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(429.3 731) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.011</tspan></text>
                </g>
                <g id="1.013" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="446.6" y1="781.9" x2="507.7" y2="781.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="390 834.1 459.5 834.1 459.5 892" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="422.4 834.1 422.4 781.9 432.5 781.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '4.1px' }}/>
                    <line x1="505.7" y1="781.9" x2="505.7" y2="892" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="459.5" y1="892" x2="505.7" y2="892" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(446.5 811.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.013</tspan></text>
                </g>
                <g id="1.157" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <text transform="translate(341.7 896.4) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.157</tspan></text>
                </g>
                <g id="1.016" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <polyline points="169.8 812.1 169.8 892 335.1 892 335.1 811.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="318.9" y1="823" x2="335.3" y2="823" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="207.3" y1="823" x2="291" y2="823" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="823" x2="193.3" y2="823" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(228.1 863.7) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.016</tspan></text>
                </g>
                <g id="1.045" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="55.9" y1="727.4" x2="118.1" y2="727.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="139.9" y1="727.4" x2="139.9" y2="790.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="139.9" y1="816" x2="139.9" y2="890" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="55.9" y1="890" x2="141.2" y2="890" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="727.4" x2="57.3" y2="889.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.3 814.9) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.045</tspan></text>
                </g>
                <g id="1.046" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="116.1" y1="672.9" x2="116.1" y2="684" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="55.9 725.4 116.7 725.4 116.7 725.4 116.7 697.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="56 672.4 139.9 672.4 139.9 683.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="672.9" x2="57.3" y2="726" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(63.1 704) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.046</tspan></text>
                </g>
                <g id="1.047" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="139.9" y1="618.4" x2="139.9" y2="628.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="139.9" y1="643.3" x2="139.9" y2="670.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="56" y1="670.9" x2="141.1" y2="670.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="618.4" x2="57.3" y2="670.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.2 649.9) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.047</tspan></text>
                    <line x1="141.3" y1="618.4" x2="55.8" y2="618.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <g id="1.048" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="139.9" y1="544.9" x2="139.9" y2="592.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="55.8" y1="582.5" x2="139.9" y2="582.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="120.9" y1="544.9" x2="56.2" y2="544.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }}/>
                    <line x1="57.3" y1="543.7" x2="57.3" y2="583.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="135" y1="544.9" x2="141.1" y2="544.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.8 568.3) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.048</tspan></text>
                </g>
                <g id="1.049" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="139.9" y1="506.7" x2="139.9" y2="513" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.2px' }}/>
                    <line x1="120.9" y1="542.9" x2="55.7" y2="542.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.6px' }}/>
                    <polyline points="139.9 526.6 139.9 542.9 135 542.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="508" x2="141.1" y2="508" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="506.7" x2="57.3" y2="543.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.6 529.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.049</tspan></text>
                </g>
                <g id="1.050" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="139.9" y1="450.6" x2="139.9" y2="461.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }}/>
                    <line x1="86.1" y1="452" x2="55.7" y2="452" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2px' }}/>
                    <text transform="translate(74.1 482.2) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.050</tspan></text>
                    <line x1="57.3" y1="452" x2="57.3" y2="508" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="102" y1="452" x2="141.1" y2="452" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                </g>
                <g id="1.052" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="86.1" y1="450" x2="55.2" y2="450" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="139.9 367.1 139.9 450 102 450" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="86.1" y1="343.2" x2="55.9" y2="343.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="141.9 315.5 141.9 343.2 102 343.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="342.3" x2="57.3" y2="450.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.7 403.5) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.052</tspan></text>
                </g>
                <g id="1.053" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="86.1" y1="341.2" x2="55.9" y2="341.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="139.9 315.5 139.9 341.2 102 341.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="55.8 261.6 139.9 261.6 139.9 299.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="57.3" y1="260.9" x2="57.3" y2="342.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(74.5 301.9) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.053</tspan></text>
                </g>
                <g id="1.056" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="87" y1="166.1" x2="87" y2="172.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="87" y1="121.4" x2="87" y2="153.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="139.9 168.1 139.9 171.8 55.2 171.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="139.9" y1="121.4" x2="139.9" y2="155.1" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(84.6 164.2)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.056</tspan></text>
                </g>
                <g id="1.029" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="169.8" y1="170.2" x2="169.8" y2="157.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3.1px' }}/>
                    <line x1="223.7" y1="208.7" x2="223.7" y2="193.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="223.7" y1="178.8" x2="223.7" y2="122.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="169.8 143.1 169.8 121.4 330 121.4 330 194.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="167.8 168.8 196.4 168.8 196.4 208.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="123.4" x2="224.6" y2="123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="196.4" y1="207.2" x2="224.6" y2="207.2" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(171.8 153.9)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.029</tspan></text>
                </g>
                <g id="1.028" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="225.8" y1="208.7" x2="225.8" y2="193.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '3px' }}/>
                    <line x1="225.8" y1="178.8" x2="225.8" y2="121.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="329.9 193.8 329.9 206.7 316 206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="225.8" y1="123.4" x2="330.3" y2="123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="329.9" y1="122.4" x2="329.9" y2="207.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(253.1 153.9)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.028</tspan></text>
                </g>
                <g id="1.001A" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="346.6" y1="206.7" x2="328.1" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="390.9" y1="206.7" x2="381.7" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.6px' }}/>
                    <line x1="332" y1="123.4" x2="332" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="391.2" y1="123.4" x2="332" y2="123.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="391.2" y1="123.4" x2="391.2" y2="206.7" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(334.2 153.9) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.001A</tspan></text>
                </g>
                <g id="1.035" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="217.1" y1="397.6" x2="217.1" y2="510.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="397.6" x2="169.8" y2="464.5" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="397.6" x2="217.1" y2="397.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="479.8" x2="169.8" y2="508.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="217.1" y1="508.9" x2="168.7" y2="508.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(167 477.2) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.035</tspan></text>
                </g>
                <g id="1.023" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="219.1" y1="508.9" x2="219.1" y2="395.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="335.3" y1="439.2" x2="335.3" y2="397.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="219.1" y1="397.6" x2="335.3" y2="397.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(249.5 455.1) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.023</tspan></text>
                </g>
                <g id="1.020" onClick={(e) => handleRoom(e.currentTarget.id)}>
                    <line x1="335.3" y1="616.2" x2="335.3" y2="701.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="335.3" y1="510.9" x2="335.3" y2="590.3" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="599.5" x2="169.8" y2="510.9" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <line x1="169.8" y1="701.3" x2="169.8" y2="614.4" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="291.7 510.9 167.7 510.9 167.7 479.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="297.9 510.9 337.4 510.9 337.4 466.6" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <polyline points="327 701.3 167.7 701.3 167.7 788.8 186 788.8" style={{ fill: 'none', stroke: '#fff', strokeMiterlimit: '10', strokeWidth: '2.8px' }}/>
                    <text transform="translate(228.8 613.6) scale(.9 1)" style={{fill: '#fff', fontFamily: 'Typ1451-Bold', fontSize: '17.1px', fontWeight: '700', letterSpacing: '0em' }}><tspan x="0" y="0">-1.020</tspan></text>
                </g>
            </g>
        </svg>
    )

}