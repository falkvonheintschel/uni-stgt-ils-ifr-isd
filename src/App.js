import React from "react"
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Layout } from './components/Layout'
import { Home } from './components/Home'
import { News }from './components/News'
import { Media }  from './components/Media'
import Team from './components/Team'
import Page from './components/Page'

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout/>}>
        <Route index element={<Home />} />
        <Route path="/page/:slug" element={<Page />} />
        <Route path="/news" element={<News />} />
        <Route path="/media" element={<Media />} />
        <Route path="/team" element={<Team />} />
      </Route>
    </Routes> 
  )
}

export default App