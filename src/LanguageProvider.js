import React, {
	useState,
	createContext,
	useCallback,
	useEffect,
	useRef
} from "react"

export const LanguageContext = createContext({
	language: "de",
	updateLanguage: () => {}
})

export const LanguageProvider = ({ children }) => {
	const [{ language }, setLanguage] = useState({
		language: "de",
	})

	const updateLanguage = useCallback(
		async (newLang) => {
			if (newLang === language) return

			setLanguage({
				language: newLang,
			})
		}, [language]
	)

	useEffect(() => {
		// console.log('language', language)
		updateLanguage(language)
	}, [language, updateLanguage])

	const context = {
		language,
		updateLanguage: updateLanguage
	}

	return (
		<LanguageContext.Provider value={context}>
			{children}
		</LanguageContext.Provider>
	)
}