import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import { initReactI18next } from 'react-i18next'
// import LanguageDetector from 'i18next-browser-languagedetector'

const fallbackLng = ['de']
const availableLanguages = ['de', 'en']

i18n.use(Backend)
    // .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        lng: 'de    ',
        fallbackLng,
        detection: {
            checkWhitelist: true
        },
        debug: false,
        whitelist: availableLanguages,
        interpolation: {
            escapeValue: false // no need for react. it escapes by default
        },
        react: {
            wait: true
        }
    })

export default i18n