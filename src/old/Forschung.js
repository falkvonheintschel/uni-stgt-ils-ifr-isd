import React from 'react';
import {Link} from 'react-router-dom';

import { useState, useEffect } from 'react';
import axios from "axios";

import { Pages } from './Pages';
// import { PageTabs } from './PageTabs';

function Forschung() {

    const [forschungContent, setForschungContent] = useState([]);

    useEffect(() => {
        axios
        .get("/json/ils-forschung.json")
        .then((res) => setForschungContent(res.data))
        .catch((err) => console.log(err));

        console.log(forschungContent)
        if (forschungContent.length > 0) {      
        }
    }, []);

    return (
        <div className="container max-w-[1280px] h-[calc(100%-8rem)] mx-auto flex flex-wrap justify-center">
            <div className="relative h-[calc(100%-86px)] w-full overflow-hidden"> 
                <Pages content={forschungContent} />
                {/* <PageTabs content={forschungContent.sectionsForschung} /> */}
            </div>

            <div className="w-full self-end">
                <div className="w-1/2 bg-white text-blue py-4">
                    <Link to="/forschung" className="block text-center text-3xl uppercase">Forschung</Link>
                </div>
            </div>
        </div>
    )
}

export default Forschung