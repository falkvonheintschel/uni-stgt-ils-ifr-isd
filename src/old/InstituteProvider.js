import React, { useState, createContext, useCallback, useEffect } from "react"
// import axios from "axios"

export const InstituteContext = createContext({
	institute: "isd",
	updateInstitute: () => {}
})

export const InstituteProvider = ({ children }) => {
	const [{ institute }, setInstitute] = useState({
		institute: "isd",
	})

	const updateInstitute = useCallback(
		async (newInstitute) => {
			if (newInstitute === institute) return

			setInstitute({
				institute: newInstitute,
			})
		}, [institute]
	)

	useEffect(() => {
		console.log('institute', institute)
		updateInstitute(institute)
	}, [institute, updateInstitute])

	const context = {
		institute,
		updateInstitute: updateInstitute
	}

	return (
		<InstituteContext.Provider value={context}>
			{children}
		</InstituteContext.Provider>
	)
}